﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Global.SaveAndLoader;
using Loc2DBlockns;
using PointType;

public class SaveState : EditorWindow{

    private string fileName = "File name...";
    private string text = "C:\\Users\\Stumpy\\Dropbox\\CityBuilder\\City Builder2\\savedStates\\";
    private string defaultLoc = "C:\\Users\\Stumpy\\Dropbox\\CityBuilder\\City Builder2\\savedStates\\";
    [MenuItem("Window/GameSave")]
    public static void ShowWindow()
    {
        EditorWindow.GetWindow(typeof(SaveState));
    }

    void OnGUI()
    {

        GUILayout.BeginHorizontal();
        GUILayout.Label("File Name: ");
        fileName = GUILayout.TextField(fileName);
        EditorGUILayout.EndHorizontal();
        GUILayout.BeginHorizontal();
        GUILayout.Label("Save Location: ");
        text = GUILayout.TextField(text);
        EditorGUILayout.EndHorizontal();
        if (GUILayout.Button("Save game")) {
            if (GameManager.GM.SaveState(text, fileName)) Debug.Log("State saved correctly");
            else Debug.Log("Error saving state");
        }
        if (GUILayout.Button("Reset Location")) { text = defaultLoc; }
        if (GUILayout.Button("Test Load file:")) 
            {
            Debug.Log(GameManager.GM.LoadState(@"C:\Users\Stumpy\Dropbox\CityBuilder\City Builder2\savedStates\FileName2334"));
        }
    }
}
