﻿using Buildingns;
using Loc2DBlockns;
using PointType;
using ResourceNs;
//using ResourceNs;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class AddBuildingButtonFunc : MonoBehaviour {

    private int c = 0;
    public InputField xLoc, yLoc;
    public Dropdown type;
    public GameManager gm;
    public int maxEmps, iMax, oMax, shopFront, delSize;
    public float timeToProduce;
    public BPlaceWidget widget;
    public int warehouseCapacity = 50;
    private Building<Loc2DBlock, Point> b;

    public void AddBuilding()
    {
        
        int t = type.value;

        ///todo: Make work with negative values properly
        if (t == 0) b = CreateIO("Farm", "Farmer", Resource.RawFood);
        else if (t == 1) b = CreateIO("Food Factory", "Factory worker", Resource.RawFood, Resource.Food);
        else if (t == 2) b = CreateWH();
        else if (t == 3) b = CreateIO("Wood Chopper", "Lumberjack", Resource.Wood);
        else if (t == 4) b = CreateIO("Stone Miner", "Miner",Resource.Stone);
        else throw new InvalidOperationException("Building index not expected");
        c++;
        State.ChangeState(GState.BPlacement);
        var w = Instantiate(widget);
        w.setInfo(b);

    }

    /// <summary>
    /// Creates a building that has an in and out resource
    /// </summary>
    /// <param name="label">The label to use to descreibe the building</param>
    /// <param name="workerName">The name of someone who works in this building</param>
    /// <param name="inR">The input resource</param>
    /// <param name="outR">The output resource</param>
    /// <returns>The created building</returns>
    private Building<Loc2DBlock, Point> CreateIO(string label, string workerName, Resource inR, Resource outR)
    {
        return Building<Loc2DBlock, Point>.NewIO(CreateIO(label, workerName, outR).iIO.Value.sIType(inR).sIMax(iMax));
    }

    /// <summary>
    /// Creates a building that outputs a resource but does not require an input resource
    /// </summary>
    /// <param name="label">The label to use to describe this building</param>
    /// <param name="workerName">The name of someone who works in this building</param>
    /// <param name="outR">The resource output</param>
    /// <returns></returns>
    private Building<Loc2DBlock, Point> CreateIO(string label, string workerName, Resource outR)
    {
        var bldName = label + " " + c;
        var emps = BldEmpl.Default.sEmpDesc(workerName).sEmpMax(maxEmps).sEmpCap(maxEmps);
        var bBase = BldBase<Loc2DBlock, Point>.Default2D.sLabel(label).sName(bldName);
        return Building<Loc2DBlock, Point>.NewIO(Buildingns.funcs.EmpToBld(Buildingns.funcs.BldToEmp(Building<Loc2DBlock, Point>.DefaultIO(new Loc2DBlock(0, 1, 1, 0), new Point(0, 0)).sCore(bBase)).sEmpDesc(workerName).sEmpMax(maxEmps).sEmpCap(maxEmps)).iIO.Value.sOType(outR).sOMax(oMax).sTimeToProduce(timeToProduce).sShopFront(shopFront).sDelSize(delSize));
    }

    private Building<Loc2DBlock, Point> CreateWH()
    {
        var emps = BldEmpl.Default.sEmpDesc("Warehouse Worker").sEmpMax(maxEmps).sEmpCap(maxEmps);
        var core = BldBase<Loc2DBlock, Point>.Default2D.sName("Warehouse " + c).sLabel("Warehouse");
        return Building<Loc2DBlock, Point>.NewWH(WHinfo<Loc2DBlock, Point>.Default2D.sE(emps).sCapacity(warehouseCapacity)).sCore(core);
    }
}
