﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BldEmpDrag : MonoBehaviour {

    public static bool dragging = false;
    public static bool dropped = false;
    public static bool swapped = false;
    public static int bldID;
    public static int bldIdx;
    public static string display;
    public GameObject image;
    public Text text;
    public Vector2 textOffset;

    private bool killme = false;
	// Use this for initialization
	void Start () {
        dragging = true;
    }
	
	// Update is called once per frame
	void Update () {
        image.transform.position = new Vector3(Input.mousePosition.x, Input.mousePosition.y);
        text.transform.position = new Vector3(Input.mousePosition.x + textOffset.x, Input.mousePosition.y + textOffset.y);
        text.text = display;
        if (Input.GetMouseButtonUp(0))
        {
            killme = true;
            dropped = true;
            //waits a frame so all GUILayouts can check if they have had object droped on them
        }
        else if (killme)
        {
            dragging = false;
            dropped = false;
            swapped = false;
            Destroy(this.gameObject);
        }
	}
}
