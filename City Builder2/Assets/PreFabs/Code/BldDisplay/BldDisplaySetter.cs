﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BldDisplaySetter : MonoBehaviour {

    private int id;
    private GameManager gm;
    private int height = 200, width=230;


    public BldEmpDrag dragEmpObject;
    public Text displayText;
    public GameObject posFollow;
    public float offsetX = 0.0f;
    public float offsetY = 0.0f;
    public GUIStyle textStyle;

    
	// Use this for initialization
	void Start () {
		
	}
	


    public void OnGUI()
    {
        var b = gm.GetBuilding(id);
        GUILayout.BeginArea(new Rect(offsetX + posFollow.transform.position.x, offsetY -posFollow.transform.position.y, height, width));

        GUILayout.Label(b.name,textStyle);

        GUILayout.BeginHorizontal();
        if (b.isEmp)
        {
            var e = b.iEmp.Value;
            GUILayout.Label("Employees: " + e.emps.Length + " of " + e.empCap + "(max:" + e.empMax + ")", textStyle);
            if (GUILayout.Button("+") && e.empCap < e.empMax) GameManager.GM.SetEmpCap(b.id, e.empCap + 1);
            if (GUILayout.Button("-") && e.empCap > 0) GameManager.GM.SetEmpCap(b.id, e.empCap - 1);
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            var l = e.emps;

            for (int i = 0; i < e.empMax; i++)
            {
                if (!l.IsEmpty)
                {
                    GUILayout.Box(l.Head.ToString());
                    l = l.Tail;
                }
                else GUILayout.Box(i < e.empCap ? "-" : "X");

                if (Input.GetMouseButtonDown(0) && !BldEmpDrag.dragging && GUILayoutUtility.GetLastRect().Contains(Event.current.mousePosition))
                {
                    Debug.Log("Starting");
                    BldEmpDrag.bldID = id;
                    BldEmpDrag.bldIdx = i;
                    BldEmpDrag.display = (i < e.emps.Length ? i.ToString() : (i < e.empCap ? "-" : "X"));
                    Instantiate(dragEmpObject);
                }
                else if (BldEmpDrag.dropped && !BldEmpDrag.swapped && GUILayoutUtility.GetLastRect().Contains(Event.current.mousePosition))
                {
                    BldEmpDrag.swapped = true;
                    if (BldEmpDrag.bldID != id) gm.Swap(id, i, BldEmpDrag.bldID, BldEmpDrag.bldIdx);
                }
            }
            GUILayout.EndHorizontal();

        }

        if (b.IsIO)
        {
            var io = b.iIO.Value;
            GUILayout.Label("Inventory: ", textStyle);
            GUILayout.BeginHorizontal();

            if (io.isSomeIType)
            {
                GUILayout.BeginVertical();
                GUILayout.Label(io.iType.Value.ToString() + " (max " + io.iMax + ")", textStyle);
                GUILayout.Box(io.iStored.ToString() + " (" + io.enRoute + " en route)");
                GUILayout.EndVertical();
            }
            if (io.isSomeOType)
            {
                GUILayout.BeginVertical();
                GUILayout.Label(io.oType.Value.ToString() + " (max " + io.oMax + ")", textStyle);
                GUILayout.Box(io.oStored.ToString() + " (" + io.inProgress + " being made)");
                GUILayout.EndVertical();
            }
            GUILayout.EndHorizontal();
        }

        if (b.IsWH)
        {
            var wh = b.iWH.Value;
            GUILayout.BeginVertical();
            foreach (var r in wh.inv)
            {
                GUILayout.BeginHorizontal();
                GUILayout.Label(r.name + " " + r.current + " (t:" + r.target + ", er:" + r.enRoute + ")");
                if (GUILayout.Button("+")) GameManager.GM.SetWHTarget(b.id, r.res, r.target + 1);
                if (GUILayout.Button("-")) GameManager.GM.SetWHTarget(b.id, r.res, r.target - 1);
                GUILayout.EndHorizontal();
            }
            GUILayout.EndVertical();

        }

        GUILayout.EndArea();
    }

    public void SetInfo(GameManager gm, int id)
    {
        this.id = id;
        this.gm = gm;
    }

    public void Close()
    {
        Destroy(this.gameObject);
    }
}
