﻿using Buildingns;
using Loc2DBlockns;
using PointType;
using ResourceNs;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingColours : MonoBehaviour {

    public static BuildingColours c;

    public Color defaultColour;
    public Color farmColour;
    public Color foodFactoryColour;
    public Color warehouseColour;
    public Color woodcutterColour;
    public Color stoneMinerColour;

	// Use this for initialization
	void Start () {
        if (c == null) c = this;
        else Debug.Log("Warning: More than 1 building Colours exist");
	}
	
    public Color GetColour(Building<Loc2DBlock,Point> bld)
    {
        if (bld.label == "Farm") return farmColour;
        else if (bld.label == "Food Factory") return foodFactoryColour;
        else if (bld.label == "Warehouse") return warehouseColour;
        else if (bld.label == "Wood Chopper") return woodcutterColour;
        else if (bld.label == "Stone Miner") return stoneMinerColour;
        else return defaultColour;
    }

}
