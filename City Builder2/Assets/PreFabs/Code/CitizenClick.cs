﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.UIElements;
using UnityEngine.UI;

public class CitizenClick : MonoBehaviour, OnClick {

    public CitDisplaySetter dispType;
    public CitizenMover cm;
    public void LeftClick()
    {
        var d = Instantiate(dispType);
        d.SetInfo(cm.GetGM(), cm.id);
    }

}
