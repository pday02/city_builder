﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CitDisplaySetter : MonoBehaviour {

    int id;
    GameManager gm;
    public Text displayText;

    public void SetInfo(GameManager gm, int id)
    {
        this.id = id;
        this.gm = gm;
    }

    public void Close()
    {
        Destroy(this.gameObject);
    }

    void Update()
    {
        //todo: Change CitDisplaySetter to use a listener rather than run every tic
        if (id != -1 && gm != null)
        {
            var c = gm.GetCitizen(id);
            displayText.text = "Name: " + c.name
                                + "\n" + (c.empStatus.IsWorking ? gm.GetBuilding(c.workplace).iEmp.Value.empDesc + " (" + gm.GetBuilding(c.workplace).name + ")" : (c.empStatus.IsUnemployed ? "Unemployed" : "Seeking employment")) 
                                + "\n" + (c.tasks.Length > 1 ? c.tasks.Head.Desc(c.tasks.Tail) : (c.tasks.Length > 0 ? c.tasks.Head.Desc() : "Idle"));
        }
    }
}
