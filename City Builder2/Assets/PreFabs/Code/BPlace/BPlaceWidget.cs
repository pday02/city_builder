﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Buildingns;
using Loc2DBlockns;
using PointType;

public class BPlaceWidget : MonoBehaviour, OnClick {

    private Building<Loc2DBlock, Point> bld;
    public MeshRenderer displayObject;
    public Color valid;
    public Color invalid;
    // Use this for initialization
    void Start () {
        State.StateChanged += DestroyObject;
	}
	
	// Update is called once per frame
	void Update () {
        var mouseLoc = XZPlane.GetMousePositionOnXZPlane();
        transform.position = new Vector3((int)mouseLoc.x + 0.5f, 0.5f, (int)mouseLoc.z + 0.5f);
        var newL = new Point((int)mouseLoc.x + 0.5, (int)mouseLoc.z + 0.5);
        //todo: Currently only works with building of size 1
        var newA = new Loc2DBlock((int) mouseLoc.x, (int) mouseLoc.x + 1, (int)mouseLoc.z + 1, (int)mouseLoc.z);
        bld = bld.sArea(newA).sLocation(newL);

        displayObject.material.color = (GameManager.GM.IsValidPlacement(bld.area) ? valid : invalid);
        	
	}

    public void setInfo(Building<Loc2DBlock,Point> b) {
        bld = b;
    }


    public void LeftClick()
    {
        GameManager.GM.AddBuilding(bld);
        State.ChangeState(GState.Free);
    }

    private void DestroyObject(object sender, GState from, GState to)
    {
        State.StateChanged -= DestroyObject;
        Destroy(this.gameObject);
    }
}
