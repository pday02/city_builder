﻿using Citizenns;
using Loc2DBlockns;
using PointType;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CitizenMover : MonoBehaviour {

    public int id=-1;
    private GameManager GM;
    public float walkingHeight = 0.4f;
	// Use this for initialization
	void Start () {
       
	}
	
	// Update is called once per frame
	void Update () {
        ///eff: This runs every tic for every citizen. Could likely be slicker.
        if (GM == null && GameManager.GM != null) GM = GameManager.GM;
        else
        {
            if (id != -1)
            {
                //Update position from gamestate 
                var p = GM.GetCitizen(id).location;
                transform.position = new Vector3( (float) p.x,walkingHeight, (float)p.y);
            }
        }
	}

    public GameManager GetGM()
    {
        return GM;
    }
}
