﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingScript : MonoBehaviour, OnClick {

    public BldDisplaySetter displayType;
    public float vOffset = 0.5f;
    public float hOffset = 0.5f;
    public MeshRenderer objectToColour;
    private int id = -1;
    private GameManager GM;


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void setID(GameManager gm, int id)
    {
        this.id = id;
        GM = gm;
        var b = GM.GetBuilding(id);
        transform.position = new Vector3(((float)b.area.left) + hOffset,vOffset, (float) b.area.bottom+hOffset);
        objectToColour.material.color = BuildingColours.c.GetColour(b);
    }

    public void LeftClick()
    {
        if (id != -1 && GM != null)
        {
            var d = Instantiate(displayType);
            d.SetInfo(GM, id);
        }
    }
}
