﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Creates a citizen on the GUI when one is created in the gamestate
/// </summary>
public class CitGUICreator : MonoBehaviour {

    private GameManager GM;
    public CitizenMover cm; 

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (GM == null && GameManager.GM != null)
        {
            GM = GameManager.GM;
            GM.CitAdded += Event_CitAdded;
        }
    }

    private void Event_CitAdded(object s, int id)
    {
        var c = Instantiate(cm);
        c.id = id;
    }
}
