﻿using UnityEngine;
using MasterGame;
using Loc2DBlockns;
using PointType;
using MasterGameImpl;
using Map2DBlockns;
using Citizenns;
using GameState;
using ResourceNs;
using Buildingns;
using Taskns;
using Global.SaveAndLoader;



public class GameManager : MonoBehaviour {

    //EVENT HANDLERS
    public event CitAddedHandler CitAdded;
    public event BldAddedHandler BldAdded;
    public event TaskCompletedHandler TaskCompleted;
    public event WorkAssignedHandler WorkAssigned;
    public event ResAbanHandler ResAbandoned;

    //MAP SETTINGS
    public int mapLeft = -100, mapTop = 100, mapBottom=-100, mapRight = 100;
    private CityMap.CityMap<Loc2DBlock,Point> map;

    //GAMESTATES
    public static GameManager GM = null;
    private MasterGame<Loc2DBlock,Point> gameRules;
    private GameState<Loc2DBlock, Point> gs;



	void Start () {
        if (GameManager.GM != null) throw new PlayerPrefsException("More than one gamemanager exists");
        else GM = this;
        map = new Map2DBlock(mapLeft, mapRight, mapTop, mapBottom);
        gameRules = new MasterGameImpl<Loc2DBlock, PointType.Point>(map);
        gs = GameState<Loc2DBlock, Point>.Empty<Loc2DBlock, Point>();
    }
	
	// Update is called once per frame
	void Update () {

        gs = gameRules.tic(Time.deltaTime, gs);
        
        var evs = Event<Loc2DBlock, Point>.Convert(gs.events);
        gs = gameRules.clearEventQueue(gs);

        var l1 = evs.cits;
        while (!l1.IsEmpty)
        {
            var c = l1.Head;
            if (CitAdded != null) CitAdded(this,c.id);
            l1 = l1.Tail;
        }

        var l2 = evs.blds;
        while (!l2.IsEmpty)
        {
            var c = l2.Head;
            if (BldAdded != null) BldAdded(this, c.id);
            l2 = l2.Tail;
        }

        var l3 = evs.taskCom;
        while (!l3.IsEmpty)
        {
            var c = l3.Head;
            if (TaskCompleted != null) TaskCompleted(this, c.Item1.id, c.Item2.ToString());
            l3 = l3.Tail;
        }

        var l4 = evs.wrkAss;
        while (!l4.IsEmpty)
        {
            var c = l4.Head;
            if (WorkAssigned != null) WorkAssigned(this, c.id);
            l4 = l4.Tail;
        }

        var l5 = evs.abanRes;
        while (!l5.IsEmpty)
        {
            var c = l5.Head;
            if (ResAbandoned != null) ResAbandoned(this, c);
            l5 = l5.Tail;
        }

    }

    public bool LoadState(string loc)
    {
        GM.gameRules.load(loc);
        return true;
    }
    public bool SaveState(string loc, string name)
    {
        Saver x = new SaverAndLoader();
        string fileName = loc + name;
        Debug.Log(fileName);
        Debug.Log(gs);
        Debug.Log(gs == null);
        return x.SaveToFile(fileName, gs);
    }

    //GAMESTATE ALTER METHODS
    public void AddCitizen(string name="Default",double startX=0.0,double startY=0.0)
    {
        var c = Citizen<Point>.Default(new Point(startX, startY));

        //todo: remove everyone always working when added
        c = c.setSeeking().setName(name);
        
        gs = gameRules.addCitizen(c, gs);
        
    }

    public void AddBuilding(Building<Loc2DBlock,Point> bld)
    {
        gs = gameRules.addBuilding(bld, gs).Item2;
    }

    public bool IsValidPlacement(Loc2DBlock loc)
    {
        return map.isBuildingValid(loc, gs);
    }

    public void Swap(int bid1, int idx1, int bid2, int idx2)
    {
        gs = gameRules.empSwap(bid1, idx1, bid2, idx2, gs);
    }

    public void SetEmpCap(int id, int cap)
    {
        gs = gameRules.empCap(id, cap, gs);
    }

    public void SetWHTarget(int id, Resource r, int n)
    {
        gs = gameRules.setWHTarget(id, r, n, gs);
    }


    //ACCESS METHODS
    public Citizen<Point> GetCitizen(int id)
    {
        return gs.citizens[id];
    }
    public Building<Loc2DBlock, Point> GetBuilding(int id) { return gs.buildings[id]; }

    public delegate void CitAddedHandler(object sender, int citID);
    public delegate void BldAddedHandler(object sender, int bldID);
    public delegate void TaskCompletedHandler(object sender, int citID, string task);
    public delegate void WorkAssignedHandler(object sender, int citID);
    public delegate void ResAbanHandler(object sender, AbandonedResource<Point> res);
    //Deal with event from listener

}

