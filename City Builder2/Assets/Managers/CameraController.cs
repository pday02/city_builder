﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour {

    public int speed;
    public int rotSpeed;
	// Use this for initialization
	void Start () {
	
	}

    void Update()
    {
        if (Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.D))
        {
            float[] dir = GetSidewaysDirection();
            transform.position += new Vector3(dir[0], 0, dir[1]);
        }
        if (Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.A))
        {
            float[] dir = GetSidewaysDirection();
            transform.position += new Vector3(dir[0] * -1.0f, 0, dir[1] * -1.0f);
        }
        if (Input.GetKey(KeyCode.DownArrow) || Input.GetKey(KeyCode.S))
        {
            float[] dir = GetForwardDirection();
            transform.position += new Vector3(dir[0] * -1.0f, 0, dir[1] * -1.0f);
        }
        if (Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.W))
        {
            float[] dir = GetForwardDirection();
            transform.position += new Vector3(dir[0],0, dir[1]);
        }
        if (Input.GetKey(KeyCode.Q)){
            Vector3 newRot = transform.rotation.eulerAngles + new Vector3(0.0f,-1.0f * rotSpeed*Time.deltaTime,0.0f);
            transform.rotation = Quaternion.Euler(newRot);
        }
        if (Input.GetKey(KeyCode.E))
        {
            Vector3 newRot = transform.rotation.eulerAngles + new Vector3(0.0f, rotSpeed * Time.deltaTime, 0.0f);
            transform.rotation = Quaternion.Euler(newRot);
        }
    }

    
    private float[] GetForwardDirection()
    {
        float rot = transform.rotation.eulerAngles.y;
        return new float[] { Mathf.Sin(Mathf.Deg2Rad * rot) * speed * Time.deltaTime, Mathf.Cos(Mathf.Deg2Rad * rot) * speed * Time.deltaTime };

    }
    private float[] GetSidewaysDirection()
    {
        float rot = transform.rotation.eulerAngles.y;
        if (rot >= 0.0f && rot <= 90.0f)
        {
            return new float[] { Mathf.Sin(Mathf.Deg2Rad * (90.0f + rot)) * speed * Time.deltaTime, Mathf.Cos(Mathf.Deg2Rad * (90.0f + rot)) * speed * Time.deltaTime };
        }
        else if (rot > 90.0f)
        {
            return new float[] { Mathf.Sin(Mathf.Deg2Rad * (90.0f + rot)) * speed * Time.deltaTime, Mathf.Cos(Mathf.Deg2Rad * (90.0f + rot)) * speed * Time.deltaTime };
            //return new float[] { Mathf.Sin(Mathf.Deg2Rad * rot) * rotSpeed * -1.0f * Time.deltaTime, Mathf.Cos(Mathf.Deg2Rad * rot) * rotSpeed * Time.deltaTime };
        }
        else if (rot < 0.0f && rot >= -90.0f)
        {
            return new float[] { Mathf.Sin(Mathf.Deg2Rad * rot) * speed * Time.deltaTime, Mathf.Cos(Mathf.Deg2Rad * rot) * speed * -1.0f * Time.deltaTime };
        }
        else
        {
            return new float[] { Mathf.Sin(Mathf.Deg2Rad * rot) * speed * -1.0f * Time.deltaTime, Mathf.Cos(Mathf.Deg2Rad * rot) * speed * -1.0f * Time.deltaTime };
        }
    }
}
