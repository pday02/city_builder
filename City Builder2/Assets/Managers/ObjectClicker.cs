﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// Detects if an object has been clicked on and, if it has, if it has an "onclick" component to call.
/// </summary>
public class ObjectClicker : MonoBehaviour {

    public OnClick c;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit, 100))
            {
                OnClick c = hit.transform.GetComponent<OnClick>();
                if (c != null) c.LeftClick();
                else Debug.Log("Object left clicked on does not implement onclick interface");
            }
        }
    }
}
