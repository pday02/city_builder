﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Logger : MonoBehaviour {

    private GameManager GM;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (GM == null && GameManager.GM != null)
        {
            GM = GameManager.GM;
            GM.CitAdded += Event_CitAdded;
            GM.BldAdded += Event_BuildingAdded;
            GM.TaskCompleted += Event_TaskComplete;
            GM.WorkAssigned += Event_WorkAssigned;
        }
	}

    private void Event_CitAdded(object sender, int id)
    {
        Debug.Log("New citizen " + id + " has been logged");
    }

    private void Event_BuildingAdded(object sender, int id)
    {
        Debug.Log("New building " + id + " has been logged");
    }

    private void Event_TaskComplete(object sender, int id, string task)
    {
        Debug.Log("Citizen " + id + " has finished task " + task);
    }

    private void Event_WorkAssigned(object sender, int id)
    {
        Debug.Log("Citizen " + id + " has started working");
    }
}
