﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum GState {Free, BPlacement}

public class State  {

    public static GState current = GState.Free;
    public static event StateChangeHandler StateChanged; 

    public static void ChangeState(GState newState)
    {
        var prev = current;
        current = newState;
        if (StateChanged != null) StateChanged(null, prev, current);
    }

    public delegate void StateChangeHandler(object sender, GState from, GState to);
}
