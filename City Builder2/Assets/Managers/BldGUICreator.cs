﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Creates a building on the gui when one is created in the gamestate
/// </summary>
public class BldGUICreator : MonoBehaviour {

    private GameManager GM;
    public BuildingScript bs;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (GM == null && GameManager.GM != null)
        {
            GM = GameManager.GM;
            GM.BldAdded += Event_BldAdded;
        }
    }

    private void Event_BldAdded(object s, int id)
    {
        var b = Instantiate(bs);
        b.setID(GM, id);
    }
}
