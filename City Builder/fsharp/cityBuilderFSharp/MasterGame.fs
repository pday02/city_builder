﻿namespace MasterGame

open Citizenns
open GameState
open Buildingns
open ResourceNs

type MasterGame<'L,'P> =
    //Adds a citizen to the game world, returning the new game state with a citizen added to the queue and the event logged in the event queue
    //The citizen will be given an ID, overwriting any that are provided in the citizen passed into the parameter
    //Emp Status:
        //If the citizen is seeking employment, it will be found a job if possible
        //If it already has a job when added, an exception will be thrown
    abstract member addCitizen: Citizen<'P> -> GameState<'L,'P> -> GameState<'L,'P>
    
    //Returns the game state with the event queue emptied
    abstract member clearEventQueue: GameState<'L,'P> -> GameState<'L,'P> 
    
    //tries to add a building. The first value in the tuple is whether it suceeded or not, the other is the updated gamstate.
    // The updated gamestate will only record the event if the building placement suceeeds
    abstract member addBuilding : Building<'L,'P> -> GameState<'L,'P> -> (bool * GameState<'L,'P>)

    abstract member findWork : int -> GameState<'L,'P> -> GameState<'L,'P>

    /// <summary>
    /// "Tics" the game forward in time, to be run every game tic, with the amount of time to pass
    /// as the first function 
    /// </summary>
    abstract member tic: float -> GameState<'L,'P> -> GameState<'L,'P>

    /// <summary>
    /// Swaps two employees over. Can either be an employee or an empty slot. Exception thrown if either building id isnt found, or index is negative or above max employees
    /// parameter 1 is the first building, p2 is the index of the employee, p3 is the second building, p4 is the index i nthe second building
    /// </summary>
    abstract member empSwap : int -> int -> int -> int -> GameState<'L,'P> -> GameState<'L,'P>

    /// <summary>
    /// Limits the number of employees a building can employ. 
    /// p1: the business ID
    /// p2: the new cap
    /// </summary>
    abstract member empCap : int -> int -> GameState<'L,'P> -> GameState<'L,'P> 

    /// <summary>
    /// Sets a target quantity for resources stored in a warehouse
    /// </summary>
    abstract member setWHTarget : int -> Resource -> int -> GameState<'L,'P> -> GameState<'L,'P>
    
    /// <summary>
    /// Loads a gamestate from a file
    /// </summary>
    abstract member load : string -> GameState<'L,'P>



    /// Create way of adding a new max 
