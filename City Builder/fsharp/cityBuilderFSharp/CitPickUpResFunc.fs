﻿module CitPickUpResFunc

open Global.Tester
open Citizenns
open Map2DBlockns
open PointType
open GameState
open Buildingns
open ResourceNs
open Global.PV
open Global.General
open Taskns
open GameState.funcs
open Buildingns.funcs

/// <summary>
/// Makes the changes to the game state for picking up resources from a building (i.e. task is Collect()) 
/// </summary>
/// <param name="cit"></param>
/// <param name="gs"></param>
let CitPickUpResFunc (cit:Citizen<'P>) (gs:GameState<'L,'P>) =
    let c = gs.citizens |> PV.tryNth cit.id |> ThrowIfNone "Citizen not found in Citizens"
    match c.tasks with
    | Task.Collect(order) :: _ ->
        let b = gs.buildings |> PV.tryNth order.building |> ThrowIfNone "Building not found in Buildings"
        if (b.location <> c.location) then {gs with citizens = gs.citizens |> PV.update c.id {c with tasks=Task.MoveTo(b.location) :: c.tasks}}
        else 
        let assigned = gs.resourcesAssigned 
                        |> Map.tryFind(order.resource) |> ThrowIfNone "None of that resource have been assigned"
                        |> Map.tryFind(b.id) |> ThrowIfNone "None of that resource has been assigned by that building"
        let newCit = {c with resCarried = (c.resCarried  |> Map.tryFind(order.resource) 
                                                |> fun x -> match x with 
                                                            | None -> c.resCarried.Add(order.resource,order.quantity) 
                                                            | Some(n) -> c.resCarried.Add(order.resource,n + order.quantity));
                            tasks = c.tasks.Tail}
        let newEvents = (TaskComplete(newCit,c.tasks.Head)) :: gs.events
        let newAssigned = gs.resourcesAssigned.Add(order.resource,gs.resourcesAssigned.[order.resource].Add(b.id,assigned - order.quantity)) 
        let newBuilding =
            match b with
            | IO(i) ->
                if ((order.quantity > assigned) || (order.quantity > i.oStored)) then failwith "Insufficient resource in building"  
                IO({i with oStored= i.oStored - order.quantity})
            | WH(i) ->
                if ( (WHStored order.resource i) < order.quantity) then failwith "Insufficient storage in warehouse"
                WH(i |> WHRemInv order.resource order.quantity)
            | Core(_) -> failwith "Core buildings cannot be collected from"
            | Blder _ -> failwith "Resources cannot be collected form a builder"
            | NotBuilt _ -> failwith "Resources cannot be collected from not built building"
        {gs with
            citizens = gs.citizens |> PV.update c.id newCit;
            buildings = gs.buildings |> PV.update b.id newBuilding;
            resourcesAssigned = newAssigned;
            events = newEvents}


    | _ -> failwith "Citizen does not have a collect order"

            

type private testCitPickUpResFunc() =
    static member Test() =
        ResetTests()
        SetTestName("CitPickUpResFunc")

        let b0 = Building.Default(Map2DBlock(0,1,1,0),{x=1.0;y=1.0})
        let i0 = match b0 with | IO(x) -> x |_ -> failwith "Error in test" 
        let i1 = {i0 with b = {i0.b with id=1; location={x=5.0;y=5.0}}; oStored=4}
        let b1 = IO(i1)
        let i2 = {i0 with b={i0.b with id=2; location={x=15.0;y=15.0}}; iMax=10; enRoute=2;iStored=3}
        let b2 = IO(i2)
        let i3 = {i1 with b={i1.b with id=3}; oStored=6}
        let b3 = IO(i3)
        let i5 = {i1 with b={i1.b with id=5}}
        let b5 = IO(i5)
        let whdef = WHinfo.Default(Map2DBlock(0,1,1,0),{x=1.0;y=1.0})
        let i4 = {whdef with stored=7; b = {i0.b with id=4; location=b1.location}; inv=whdef.inv |> PV.update (int Resource.Food) (WHInv.Zero.sC 8)}
        let b4= WH(i4)


        let c0 = Citizen<Point>.Default({x=1.0;y=1.0})
        let c1 = {c0 with id=1; location=b1.location; tasks=[Task.Collect({building=b1.id;resource=Resource.Food;quantity=3})]}
        let c2 = {c1 with id=2;tasks=[Task.Collect({building=b1.id;resource=Resource.Food;quantity=6})]}
        let c3 = {c1 with id=3;tasks=[Task.MoveTo({x=0.0;y=0.0});Task.Collect({building=b1.id;resource=Resource.Food;quantity=6})]}
        let c4 = {c1 with id=4; tasks=[Task.Collect({building=b5.id;resource=Resource.Food;quantity=4})]}
        let c10 = {c1 with id=10; tasks=[Task.Collect({building=b1.id;resource=Resource.Food;quantity=4})]}
        let c5 = {c1 with id=5; tasks=[Task.Collect({building=b1.id;resource=Resource.Food;quantity=5})]}
        let c6 = {c1 with id=6; tasks=[Task.Collect({building=b3.id;resource=Resource.RawFood;quantity=3})]}
        let c7 = {c1 with id=7; location={x= -5.0;y= -5.0};tasks=[Task.Collect({building=b3.id;resource=Resource.RawFood;quantity=3})]}
        let c8 = {c1 with id=8; tasks =[Task.Collect({building=4;resource=Resource.Food;quantity=3})]}        
        let c9 = {c8 with id=9; location={x= -5.0;y= -5.0}}

        let gs = {GameState<Map2DBlock,Point>.Empty() with
                    citizens = PV.ofSeq [|c0;c1;c2;c3;c4;c5;c6;c7;c8;c9|]
                    buildings = PV.ofSeq [|b0;b1;b2;b3;b4|]
                    resourcesAvailable = Map([  (Resource.RawFood,   Map([(0,10);(1,4);(3,3)]));
                                                (Resource.Food,      Map([(0,9);(1,6)]))])
                    resourcesAssigned = Map([   (Resource.RawFood,   Map([(0,2);(1,3);(3,3)]));
                                                (Resource.Food,      Map([(0,4);(1,5);(4,10)]))])
                    }
        
        try 
            ignore(CitPickUpResFunc c2 gs)
            Test "Exception thrown if not enough resources are not assigned" false
        with
        | _ -> Test "Exception thrown if not enough resources are not assigned" true
            
        try
            ignore(CitPickUpResFunc c4 gs)
            Test "Exception thrown if building ID doesnt exist" false
        with
        | _ -> Test "Exception thrown if building ID doesnt exist" true

        try
            ignore (CitPickUpResFunc c10 gs)
            Test "Exception thrown if citizen ID doesnt exist" false
        with
        | _ -> Test "Exception thrown if citizen ID doesnt exist" true

        try
            ignore (CitPickUpResFunc c3 gs)
            Test "Exception thrown if Citizen task is not Collect" false
        with
        | _ -> Test "Exception thrown if Citizen task is not Collect" true
        
        try
            ignore (CitPickUpResFunc c5 gs)
            Test "Exception thrown if output storage does not contain enough" false
        with
        | _ -> Test "Exception thrown if output storage does not contain enough" true
        
        let gs2 = CitPickUpResFunc c1 gs
        let c_out = gs2.citizens.[c1.id]
        Test "Resources put in citizens inventory" (c_out.resCarried.[Resource.Food] = 3)

        Test "If sucessful, citizens task is removed" (c_out.tasks = c1.tasks.Tail)
        
        Test "Resources removed from resources assigned" (gs2.resourcesAssigned.[Resource.Food].[1] = 2)
        
        Test "Quantity removed from oStored" (match gs2.buildings.[b1.id] with | IO(i) -> i.oStored = 1 | _ -> false)
        
        let gs3 = CitPickUpResFunc c6 gs
        Test "Able to remove all assigned if collecting removes them all" 
            ((not (gs3.resourcesAssigned.[Resource.RawFood].ContainsKey(3))) || (gs3.resourcesAssigned.[Resource.RawFood].[3]=0))
        
        Test "Event added to queue if its complete" 
            (gs3.events = [TaskComplete((gs3.citizens.[6],Task.Collect({building=b3.id;resource=Resource.RawFood;quantity=3})))])

        let gs4 = CitPickUpResFunc c7 gs
        Test "If building location is not the same as the citizens, then instead add a move to the front of the task list" 
            (gs4.citizens.[7].tasks = Task.MoveTo(b3.location) :: [Task.Collect({building=b3.id;resource=Resource.RawFood;quantity=3})])
        
        let gswh1 = CitPickUpResFunc c8 gs
        let gswh1_sol = {gs with resourcesAssigned = Map([(Resource.RawFood,   Map([(0,2);(1,3);(3,3)])); (Resource.Food,Map([(0,4);(1,5);(4,7)]))])}
                            |> SetGSBld 4 (WH({i4 with stored=i4.stored - 3} |> WHSetInv (Resource.Food) (WHInv.Zero.sC 5)))
                            |> SetGSCit 8 ({c8 with resCarried=Map([(Resource.Food,3)]); tasks=c1.tasks.Tail})
                            |> AddEvent (TaskComplete({c8 with resCarried=Map([(Resource.Food,3)]); tasks=c1.tasks.Tail},c8.tasks.Head))
        TestEq "WH goods sucessfuly taken" gswh1 gswh1_sol
        
        let gswh2 = CitPickUpResFunc c9 gs
        let gswh2_sol = gs |> SetGSCit 9 {c9 with tasks=MoveTo(b1.location):: c9.tasks}
        Test "If cit not at right location of WH, then moveto task is given" (gswh2 = gswh2_sol) 
        
        

        PrintTests()



type UnitTests =
    static member run() =
        testCitPickUpResFunc.Test()
