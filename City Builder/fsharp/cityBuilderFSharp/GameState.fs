﻿namespace GameState

open Citizenns
open Buildingns
open Taskns
open ResourceNs
open Global.PV
open GameSettings
open Global.General

type Events<'L,'P> = {cits:Citizen<'P> list; blds:Building<'L,'P> list; wrkAss:Citizen<'P> list; taskCom:(Citizen<'P> * Task<'P>) list; abanRes: AbandonedResource<'P> list; constructed:Building<'L,'P> list}

type Event<'L,'P> = 
    | NewCitizen of Citizen<'P> 
    | NewBuilding of Building<'L,'P> 
    | AssignedWork of Citizen<'P> 
    | TaskComplete of (Citizen<'P> * Task<'P>)
    | ResourceDropped of AbandonedResource<'P>
    | ARCollected of AbandonedResource<'P>
    | BuildingConstructed of Building<'L,'P>
    with
    static member Convert (events: Event<'L,'P> list) =
        let rec convertAux evs cits blds wrk task abR cAR con =
            match evs with
            | [] -> {cits=cits;blds=blds;wrkAss=wrk;taskCom=task;abanRes=abR;constructed = con}
            | NewCitizen(c) :: _ -> convertAux evs.Tail (c :: cits) blds wrk task abR cAR con
            | NewBuilding(b) :: _ -> convertAux evs.Tail (cits) (b::blds) wrk task abR cAR con
            | AssignedWork(c) :: _ -> convertAux evs.Tail (cits) (blds) (c::wrk) task abR cAR con
            | TaskComplete(t) :: _ -> convertAux evs.Tail (cits) (blds) (wrk) (t::task) abR cAR con
            | ResourceDropped(a) :: _ -> convertAux evs.Tail (cits) (blds) (wrk) (task) (a::abR) cAR con
            | ARCollected(r) :: _ -> convertAux evs.Tail (cits) (blds) (wrk) (task) (abR) (r::cAR) con
            | BuildingConstructed(b) :: _ ->convertAux evs.Tail (cits) (blds) (wrk) (task) (abR) (cAR) (b::con)

        convertAux events List.empty List.empty List.empty List.empty List.empty List.empty List.empty

//idea: Have science and culture separate. Start off with no technology. Have tasks that have to be completed to advance technology.
//idea: When a new leader comes into power, you can select your mission for this leader, which will give you a goal.
//idea: Geniuses in different areas will allow unlocking of unique technologoes, or culture for your people.
//idea: to start, your people worship aspects of nature. The weather they observe and criteria leads them to prefer some over others.
//idea: Geniuses can be religious, and start a religion for your people.
type GameState<'L,'P> = 
    {
        citizens:Citizen<'P> PV
        events:Event<'L,'P> list
        buildings:Building<'L,'P> PV
        beingBuilt: int list
        resourcesAvailable : Map<Resource, Map<int,int>>
        resourcesAssigned : Map<Resource, Map<int,int>> //1st int of the nested map is the building ID its assigned from
        settings : GameSettings
    } 
    with
        static member Empty() = {   
                                    citizens=PV.empty;
                                    events=List.empty
                                    buildings=PV.empty
                                    resourcesAvailable = Map.empty
                                    resourcesAssigned = Map.empty
                                    settings = GameSettings.Default()
                                    beingBuilt = List.empty}
        member this.getCit id = this.citizens |> PV.tryNth id |> ThrowIfNone ("Citizen ID " + string id  + " not found in gamestate")
        member this.getBld id = this.buildings |> PV.tryNth id |> ThrowIfNone ("Building ID " + string id + " not found in gamestate")

module funcs =        
    let SetGSCit id c gs = {gs with citizens = gs.citizens |> PV.tryUpdate id c |> ThrowIfNone ("Citizen " + string(id) + " not found in gamestate")}
    let SetGSBld id b gs = {gs with buildings = gs.buildings |> PV.tryUpdate id b |> ThrowIfNone ("Building " + string(id) + " not found in gamestate")}           
    
    
    let GetGSCit id gs = gs.citizens |> PV.tryNth id |> ThrowIfNone ("Citizen ID " + string id  + " not found in gamestate")
    let SetGSCitf id f gs = {gs with citizens = gs.citizens |> PV.update id (f(GetGSCit id gs))}
    let GetGSBld id gs = gs.buildings |> PV.tryNth id |> ThrowIfNone ("Building ID " + string id + " not found in gamestate")
    let SetGSBldf id f gs = {gs with buildings = gs.buildings |> PV.update id (f(GetGSBld id gs))}
    let AddEvent e gs = {gs with events = e :: gs.events}
    let SetGSRAs m gs = {gs with resourcesAssigned =m}
    let SetGSRAv m gs = {gs with resourcesAvailable = m}

    /// <summary>
    /// Adds a resource to the avialable resources for a gamestate
    /// </summary>
    /// <param name="id"></param>
    /// <param name="r"></param>
    /// <param name="n"></param>
    /// <param name="gs"></param>
    let GSAddAv id r n gs = 
        match gs.resourcesAvailable.ContainsKey r with
        | true -> 
            match gs.resourcesAvailable.[r].ContainsKey id with
            | true -> 
                let newVal = gs.resourcesAvailable.[r].[id] + n
                gs |> SetGSRAv (gs.resourcesAvailable.Add(r,gs.resourcesAvailable.[r].Add(id,newVal)))
            | false -> gs |> SetGSRAv (gs.resourcesAvailable.Add(r,gs.resourcesAvailable.[r].Add(id,n)))
        | false -> gs |> SetGSRAv (gs.resourcesAvailable.Add(r,Map([id,n])))

    let GSRemAv id r n gs =
        match gs.resourcesAvailable.ContainsKey r with
        | true -> 
            match gs.resourcesAvailable.[r].ContainsKey id with
            | true -> 
                let newVal = gs.resourcesAvailable.[r].[id] - n
                gs |> SetGSRAv (gs.resourcesAvailable.Add(r,gs.resourcesAvailable.[r].Add(id,newVal)))
            | false -> failwith "Resource can't be removed as not present"
        | false -> failwith "Reosurce cant be rmeoved as not present"

    let GSAddAss id r n gs = 
        match gs.resourcesAssigned.ContainsKey r with
        | true -> 
            match gs.resourcesAssigned.[r].ContainsKey id with
            | true -> 
                let newVal = gs.resourcesAssigned.[r].[id] + n
                gs |> SetGSRAs (gs.resourcesAssigned.Add(r,gs.resourcesAssigned.[r].Add(id,newVal)))
            | false -> gs |> SetGSRAs (gs.resourcesAssigned.Add(r,gs.resourcesAssigned.[r].Add(id,n)))
        | false -> gs |> SetGSRAs (gs.resourcesAssigned.Add(r,Map([id,n])))

    let GSRemAss id r n gs =
        match gs.resourcesAssigned.ContainsKey r with
        | true -> 
            match gs.resourcesAssigned.[r].ContainsKey id with
            | true -> 
                let newVal = gs.resourcesAssigned.[r].[id] - n
                gs |> SetGSRAs (gs.resourcesAssigned.Add(r,gs.resourcesAssigned.[r].Add(id,newVal)))
            | false -> failwith "Resource can't be removed as not present"
        | false -> failwith "Reosurce cant be rmeoved as not present"

    let GetGSAvail r gs = 
        match gs.resourcesAvailable.TryFind r with
        | Some(inv) -> inv
        | None -> Map.empty

    /// <summary>
    /// Finds a source of a desired resource
    /// </summary>
    /// <param name="r">The resource sought</param>
    /// <param name="n">The volume sought</param>
    /// <param name="gs">The game state</param>
    let FindResSource r n gs =
        match gs.resourcesAvailable.TryFind r with
        | None -> None
        | Some(inv) -> inv |> Map.tryPick(fun k v -> if (v >= n) then Some(k) else None)

    /// <summary>
    /// Returns whether a building being built can be worked on, either it requires resources which are available or it has all resources (i.e. can be worked on)
    /// </summary>
    /// <param name="gs"></param>
    let GetGSCanBuild gs =

        let resAvail (r:Resource) (m:MatInv) =  if ((m.req - m.acq - m.enR > 0 && ((FindResSource r 1 gs) |> Option.isSome))) then Some(r) else None
        let anyResAvail(m: Map<Resource,MatInv>) = m |> Map.tryPick(resAvail) |> Option.isSome
        let allResourcesGathered (m: Map<Resource,MatInv>) = m |> Map.tryPick(fun k v -> if (v.acq <> v.req) then Some(k) else None) |> Option.isNone

        let CanWork b = 
            match b with
            | NotBuilt(i,bld) ->  if ((i.res |> anyResAvail) || (i.res |> allResourcesGathered)) then Some(b.id) else None
            | _ -> None

        gs.beingBuilt |> List.tryFind(fun id -> gs.buildings.[id] |> CanWork |> Option.isSome)