﻿namespace CityMap

open GameState
open Citizenns

///Represents the map of the city terrain, providing functions to inform key information on the map 
type CityMap<'L,'P>=
    ///States whether a building can be placed in the provided location
    abstract member isBuildingValid : 'L -> GameState<'L,'P> -> bool

    /// <summary>
    /// Returns the new posiiton of a citizen moving towrds a point
    /// </summary>
    abstract member moveCit : float -> Citizen<'P> -> GameState<'L,'P> -> 'P