﻿namespace Loc2DBlockns

///A location type to be used on a map that is a 2D grid
type Loc2DBlock = {left:int; right:int; top:int; bottom:int}
    with 
        static member Default= {left=0;right=1;top=1;bottom=0}

