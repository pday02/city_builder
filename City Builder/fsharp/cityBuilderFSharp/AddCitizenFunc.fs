﻿module AddCitizenFunc

open GameState
open Global.Tester
open Citizenns
open Global.PV

let addCitizen cit gs =
    {gs with 
        citizens = PV.conj {cit with id= gs.citizens.Length} gs.citizens
        events = NewCitizen({cit with id= gs.citizens.Length}) :: gs.events}

type private testAddCitizen() =
    static member Test() =
        ResetTests()
        SetTestName("AddCitizen function")
        let gs1:GameState<int,int> = GameState<int,int>.Empty()
        let cit1 = {Citizen<int>.Default(0) with name="Citizen 1"}
        let gs2 = addCitizen  cit1 gs1
        let cit2 = {Citizen<int>.Default(0) with name="Citizen 2"}
        let gs3 = addCitizen cit2 gs2
        Test "Adding a citizen to an empty citizen list makes it the citizen list" (gs2.citizens = PV.ofSeq [|{cit1 with id=0}|])
        Test "Adding a citizen to a non empty citizen list adds it to the end of the list" (gs3.citizens = PV.ofSeq [|{cit1 with id=0};{cit2 with id=1}|])
        Test "Adding a citizen to an empty new citizen list makes it to the new citizen list" (gs2.events = [NewCitizen({cit1 with id=0})])
        Test "Adding a citizen to a non empty new citizen list is added to the list" (gs3.events = [NewCitizen({cit2 with id=1});NewCitizen({cit1 with id=0})])
        Test "Only the event list and citizen list is altered" ({gs2 with citizens=PV.empty; events = List.empty} = gs1 && {gs3 with citizens=PV.empty; events = List.empty} = gs1 )
        PrintTests()


type UnitTests =
    static member run() =
        testAddCitizen.Test()
