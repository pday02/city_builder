﻿module CitDepositResFunc

open Global.Tester
open Citizenns
open GameState
open Buildingns
open Map2DBlockns
open PointType
open ResourceNs
open Global.PV
open Global.General
open Buildingns.funcs
open Taskns
open GameState.funcs
open Loc2DBlockns

/// <summary>
/// Makes the changes to the gamestate to deposit resources at a building (i.e. task is deposit)
/// </summary>
/// <param name="cit"></param>
/// <param name="gs"></param>
let CitDepositResFunc  (cit:Citizen<'P>) (gs:GameState<'L,'P>) = 
    let c = gs.citizens |> PV.tryNth cit.id |> ThrowIfNone "Citizen not found in Citizens"
    match c.tasks with
    | Task.Deliver(order) :: _ ->
        let b = gs.buildings |> PV.tryNth order.building |> ThrowIfNone "Building not found in Buildings"
        let citAvail = c.resCarried |> Map.tryFind(order.resource) |> ThrowIfNone "Citizen doesnt have required resource in inventory"
        if (citAvail < order.quantity) then failwith "Citizen has insufficent resource quantity to deposit"
        if (b.location <> c.location) then {gs with citizens = gs.citizens |> PV.update c.id {c with tasks=Task.MoveTo(b.location) :: c.tasks}}
        else
            let newCit = {c with 
                            resCarried = c.resCarried.Add(order.resource, citAvail - order.quantity);
                            tasks = c.tasks.Tail}
            let newEvents = TaskComplete(newCit,c.tasks.Head) :: gs.events
            let temp_gs = {gs with 
                            citizens = gs.citizens |> PV.update c.id newCit; 
                            events = newEvents}
            match b with
            | IO(i) ->
                if (order.quantity > i.enRoute) then failwith "More goods being delivered than was registered in enRoute"
                if (i.iMax - i.enRoute - i.iStored < 0) then failwith "Building capacity would be exceeded if delivery made"
                let newBld = IO({i with
                                    enRoute = i.enRoute - order.quantity;
                                    iStored = i.iStored + order.quantity                            
                                })
                temp_gs |> SetGSBld b.id newBld 
            | WH(i) ->
                if (order.quantity > i.enRoute) then failwith "More goods being delivered than was registered in enRoute"
                temp_gs |> SetGSBld b.id (WH(i |> WHAddInv order.resource order.quantity |> WHRemER order.resource order.quantity))
                        |> GSAddAv b.id order.resource order.quantity     
            | NotBuilt(i,b) -> temp_gs |> SetGSBld b.id (NotBuilt(i |> NBErToAcq order.resource order.quantity,b))           
            | Core(_) -> failwith "Resources cannot be delivered to a core building"
            | Blder _ -> failwith "Resources cannot be delivered to a builder"
    | _ -> failwith "Citizen does not have a deliver order"

type private testCitDepositResFunc() =
    static member Test() =
        ResetTests()
        SetTestName("CitDepositResFunc")

        let b0 = Building.Default({left=0;right=1;top=1;bottom=0},{x=1.0;y=1.0})
        let i0 = match b0 with | IO(x) -> x | _ -> failwith "Error in test data"
        let i1 = {i0 with b={i0.b with id=1; location={x=5.0;y=5.0}}; oStored=4}
        let b1 = IO(i1)
        let i2 = {i0 with b={i0.b with id=2; location={x=15.0;y=15.0}}; iMax=10; enRoute=2;iStored=3}
        let b2 = IO(i2)
        let i3 = {i2 with b={i2.b with id=3}; enRoute=6;iMax=7}
        let b3 = IO(i3)
        

        //WareHouses
        let whdef = WHinfo<Loc2DBlock,Point>.Default2D
        let i4 = {whdef with b={whdef.b with location={x=20.0;y=20.0}}; capacity=20} 
                    |> WHInfoSid 4
                    |> WHAddInv Resource.Food 5
                    |> WHAddER Resource.Food 10

        let b4 = WH(i4)



        let c0 = Citizen<Point>.Default({x=1.0;y=1.0})
        //c1 will work
        let c1 = {c0 with id=1; location=b2.location; 
                        tasks=[Task.Deliver({building=b2.id;resource=Resource.Food;quantity=2})];
                        resCarried = Map([(Resource.RawFood,1);(Resource.Food,8)])}
        //Used for not enough in EnRoute
        let c2 = {c1 with id=2;location=b2.location; tasks=[Task.Deliver({building=b2.id;resource=Resource.Food;quantity=6})];resCarried=Map([(Resource.Food,20)])}
        //Used for building not having capaicty to receive goods
        let c3 = {c2 with id=3;tasks=[Task.Deliver({building=b3.id;resource=Resource.Food;quantity=6})]}
        //Used for Building ID not existing
        let c4 = {c1 with id=4;tasks= [Task.Deliver({building=1000;resource=Resource.Food;quantity=6})]}
        //Used for citgizen not existing
        let c_ne = {c1 with id = 1000}
        //Used for citizen task not being a deposit
        let c5 = {c1 with id=5; tasks=[Task.Collect({building=b1.id;resource=Resource.Food;quantity=3})]}
        //Used for not having enough in its storage
        let c6 = {c1 with id=6; resCarried=Map([(Resource.Food,1)])}
        //Used for building location not being the same as citizen
        let c7 = {c1 with id=7; location={x=100.0;y=100.0}}
        //Warehouse deliverer
        let c8 = {c1 with id=8;location={x=100.0;y=100.0};  tasks=[Task.Deliver({building=4;resource=Resource.Food;quantity=6})]}
        //Warehouse not in right location
        let c9 = {c8 with id=9; location=b4.location;}


        let gs = {GameState<Map2DBlock,Point>.Empty() with
                    citizens = PV.ofSeq [|c0;c1;c2;c3;c4;c5;c6;c7;c8;c9|]
                    buildings = PV.ofSeq [|b0;b1;b2;b3;b4|]
                    resourcesAvailable = Map([  (Resource.RawFood,   Map([(0,10);(1,4);(3,3)]));
                                                (Resource.Food,      Map([(0,9);(1,6);(4,3)]))])
                    resourcesAssigned = Map([   (Resource.RawFood,   Map([(0,2);(1,3);(3,3)]));
                                                (Resource.Food,      Map([(0,4);(1,5);(4,2)]))])
                    }
        
        try 
            ignore(CitDepositResFunc c2 gs)
            Test "Exception thrown if not enough resource in EnRoute" false
        with
        | _ -> Test "Exception thrown if not enough resource in EnRoute" true
            
        try
            ignore(CitDepositResFunc c3 gs)
            Test "Exception thrown is target building doesn't have capacity to receive goods" false
        with
        | _ -> Test "Exception thrown is target building doesn't have capacity to receive goods" true

        try
            ignore(CitDepositResFunc c4 gs)
            Test "Exception thrown if building ID doesnt exist" false
        with
        | _ -> Test "Exception thrown if building ID doesnt exist" true

        try
            ignore (CitDepositResFunc c_ne gs)
            Test "Exception thrown if citizen ID doesnt exist" false
        with
        | _ -> Test "Exception thrown if citizen ID doesnt exist" true

        try
            ignore (CitDepositResFunc c5 gs)
            Test "Exception thrown if Citizen task is not Deposit" false
        with
        | _ -> Test "Exception thrown if Citizen task is not Deposit" true
        
        try
            ignore (CitDepositResFunc c6 gs)
            Test "Exception thrown if citizen doesn't have enough of the resource in its storage" false
        with
        | _ -> Test "Exception thrown if citizen doesnt have enough of the resource in its storage" true
        
        let gs2 = CitDepositResFunc c1 gs
        let c_out = gs2.citizens.[c1.id]
        let b_out = match gs2.buildings.[b2.id] with | IO(x) -> x | _-> failwith "error in test data" 
        Test "Resources removed from citizens inventory" (c_out.resCarried.[Resource.Food] = 6)

        Test "Resources not removed remain unaffected in citizen inventory" (c_out.resCarried.[Resource.RawFood] = 1)

        Test "If sucessful, citizens task is removed" (c_out.tasks = c1.tasks.Tail)
        
        Test "Resources added to building inventory" (b_out.iStored = 5)
        
        Test "Resources removed from EnRoute Amount" (b_out.enRoute = 0)
                
        Test "Event added to queue if its complete" 
            (gs2.events = [TaskComplete((c_out,c1.tasks.Head))])

        let c_out_2 = (CitDepositResFunc c7 gs).citizens.[c7.id]
        Test "If building location is not the same as the citizens, then instead add a move to the front of the task list" 
            (c_out_2.tasks = Task.MoveTo(b2.location) :: c7.tasks)

        let gs_wh_1 = CitDepositResFunc c8 gs
        let gs_wh_1_sol = gs |> SetGSCit 8 {c8 with tasks=Task.MoveTo(b4.location) :: c8.tasks} 

        TestEq "Warehouse adds travel to point when cit deliverying but not in the right place"
            gs_wh_1
            gs_wh_1_sol

        let gs_wh_2 = CitDepositResFunc c9 gs
        let new_c9 = {c9 with tasks=c9.tasks.Tail; resCarried=Map([(Resource.RawFood,1);(Resource.Food,2)])}
        let gs_wh_2_sol = gs    |> SetGSCit 9 new_c9
                                |> SetGSBld 4 (WH(i4 |> WHAddInv Resource.Food 6 |> WHRemER Resource.Food 6))
                                |> SetGSRAv (Map([  (Resource.RawFood,   Map([(0,10);(1,4);(3,3)])); (Resource.Food,Map([(0,9);(1,6);(4,9)]))]))
                                |> AddEvent (TaskComplete(new_c9, c9.tasks.Head)) 
        TestEq "WH accepts delivery correctly" 
            gs_wh_2
            gs_wh_2_sol

        PrintTests()



type UnitTests =
    static member run() =
        testCitDepositResFunc.Test()
