﻿module CapEmployeeFunc

open Global.Tester
open Global.PV
open Global.General
open GameState
open GameState.funcs
open Citizenns
open Buildingns
open CancelTaskFunc
open AddBuildingFunc
open FindWorkFunc
open Citizenns.funcs
open Buildingns.funcs
open Taskns

/// <summary>
/// Sets a cap on the number of employees for a business
/// </summary>
/// <param name="cancelTask">A function that cancels a citizens tasks</param>
/// <param name="findCitWork">A function that will find a citizen new employment</param>
/// <param name="findEmps">A function that will find a building new employees</param>
/// <param name="id">The id of the business to cap the employees of</param>
/// <param name="cap">The new cap</param>
/// <param name="gs">The current gamestate</param>
let CapEmployeeFunc cancelTask findCitWork findEmps id cap (gs:GameState<'L,'P>)  = 
    let b = gs |> GetGSBld id |> BldToEmp
    let newCap = min (max 0 cap) b.empMax
    let newGSStart = gs |> SetGSBld id (b.sEmpCap(newCap) |> EmpToBld)
    if (newCap < b.emps.Length) then
        let rec CutEmpsAux id inGS =
            let bld = inGS |> GetGSBld id |> BldToEmp
            if (bld.empCap >= bld.emps.Length) then inGS
            else
                let citGo, t = bld.emps.Head, bld.emps.Tail
                let cit = inGS |> GetGSCit citGo  
                let nextgs = inGS 
                                |> cancelTask citGo 
                                |> SetGSCitf citGo SetSeeking 
                                |> SetGSBldf id (fun x -> x |> BldToEmp |> EmpSetEmps t |> EmpToBld) 
                                |> findCitWork citGo 
                CutEmpsAux id nextgs
        CutEmpsAux id newGSStart 
    elif (b.empCap > b.emps.Length) then newGSStart
    else
        let n = newCap - b.emps.Length
        let newEmps = findEmps newGSStart n
        let InsertWorkEvent id gs = AddEvent (AssignedWork((GetGSCit id gs))) gs
        let UpdateEmp bID gs cID =
            gs  |> SetGSBldf bID (fun b -> (b |> BldToEmp).sEmps (cID :: (b |> BldToEmp).emps) |> EmpToBld) 
                |> SetGSCitf cID (fun c -> {c with empStatus=Working(bID)})
                |> InsertWorkEvent cID
        newEmps |> List.fold(UpdateEmp id) newGSStart
        

type private testCapEmployeeFunc() =
    static member Test() =
        ResetTests()
        SetTestName("CapEmployeeFunc")

        let c0 : Citizen<int> = {Citizen<int>.Default(1) with id=0; empStatus=Working(0); tasks=[Task.Idle]}
        let c1 = {c0 with id=1}
        let c2 = {c0 with id=2; tasks=[Task.MoveTo(1)]}
        let c3 = {c0 with id=3}

        let i0 = {IOinfo.Default(1,1) with b = {IOinfo.Default(1,1).b with id=0}; e={IOinfo.Default(1,1).e with  emps=[3;2;1;0]; empMax=4; empCap=4}}
        let b0 = IO(i0)
        let i1 = {i0 with b = {i0.b with id=1}; e={i0.e with emps=[];empMax=4; empCap=4}}
        let b1 = IO(i1)

        let gs = {GameState<int,int>.Empty() with citizens = PV.ofSeq [c0;c1;c2;c3]; buildings=PV.ofSeq [b0;b1]}

        let testFunc : int -> int -> GameState<int,int> -> GameState<int,int> = CapEmployeeFunc CancelTaskFunc FindWorkFunc GetNewEmps

        let gs1 = testFunc 0 2 gs
        Test "Employees more than the new cap are removed and set to seeking work" 
            (
                gs1.buildings.[0] = IO({i0 with e={i0.e with empCap=2;emps=[1;0]}}) &&
                gs1.buildings.[1] = IO({i1 with e={i1.e with emps=[2;3]}}) &&
                gs1.citizens.[2] = {c2 with empStatus=Working(1); tasks=[Task.Idle]} &&
                gs1.citizens.[3] = {c3 with empStatus=Working(1)}
            )

        let gs2 = gs1 |> testFunc 1 0 |> testFunc 0 3
        Test "If cap is increased, it will check to add new employees"
            (
                gs2.buildings.[0] = IO({i0 with e={i0.e with empCap=3; emps=[2;1;0]}}) &&
                gs2.buildings.[1] = IO({i1 with e={i1.e with empCap=0; emps=[]}}) &&
                gs2.citizens.[2] = {c2 with tasks=[Task.Idle]} &&
                gs2.citizens.[3] = {c3 with empStatus = Seeking}
            )

        PrintTests()



type UnitTests =
    static member run() =
        testCapEmployeeFunc.Test()
