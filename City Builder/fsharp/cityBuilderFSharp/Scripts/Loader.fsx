﻿
#I __SOURCE_DIRECTORY__
#load "load-project-debug.fsx"

open Global.SaveAndLoader.funcs
open GameState
open Loc2DBlockns
open PointType

open System.Reflection
open Global.PV
open Global.General

open Buildingns
open WHFindCollectResource


let gsl = Load<GameState<Loc2DBlock,Point>> @"C:\Users\Stumpy\Dropbox\CityBuilder\City Builder2\savedStates\not_wh"
let gs = gsl :?> GameState<Loc2DBlock,Point>


gs.buildings

let v = match (gs.buildings |> PV.tryNth 1 |> ThrowIfNone "Building ID not found") with
            | WH(i) ->  i

v.inv
let CheckPossible r x=
    let CheckIndividual n gs bid stck =
        match GetGSBld bid gs with
        | IO(io) -> if ((n >= io.delSize) && ( (stck - io.shopFront) >= io.delSize)) then Some((bid,io.delSize)) else None
        | WH(_) -> None //todo: Allow arehouses to trade with eachother
        | Core(_) -> failwith "Core building cannot acquire resources"                 
    let maxAdd = WHFSRes r wh
    if (maxAdd <= 0) then None
    else
        match GetGSAvail r gs |> Map.tryPick(CheckIndividual maxAdd gs) with
        | Some((id,size)) -> Some(r,size,id)
        | _ -> None 