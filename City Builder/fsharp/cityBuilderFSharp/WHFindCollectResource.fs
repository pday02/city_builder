﻿module WHFindCollectResource

open Global.Tester
open ResourceNs
open Buildingns
open Buildingns.funcs
open GameState
open Global.PV
open GameState.funcs

let WHFindCollectResource wh gs = 
    let CheckPossible r x=
        let CheckIndividual n gs bid stck =
            match GetGSBld bid gs with
            | IO(io) -> if ((n >= io.delSize) && ( (stck - io.shopFront) >= io.delSize)) then Some((bid,io.delSize)) else None
            | WH(_) -> None //todo: Allow arehouses to trade with eachother
            | NotBuilt _ -> failwith "Not built buildings should not have resources available"
            | Blder _ -> failwith "Builders should not have resources available"
            | Core(_) -> failwith "Core building cannot acquire resources"  
                           
        let maxAdd = WHFSRes r wh
        if (maxAdd <= 0) then None
        else
            match GetGSAvail r gs |> Map.tryPick(CheckIndividual maxAdd gs) with
            | Some((id,size)) -> Some(r,size,id)
            | _ -> None 
    
    if ((wh.capacity - wh.enRoute - wh.stored) <= 0) then None
    else
        let wTarget, woTarget = gs.resourcesAvailable |> Map.partition(fun r v -> WHUnusedT r wh > 0)
        match wTarget |> Map.tryPick(CheckPossible) with
        | Some(r, n, fromId) -> Some(r,n,fromId,wh.b.id)
        | None -> match woTarget |> Map.tryPick(CheckPossible) with
                    | Some(r, n, fromId) -> Some(r,n,fromId, wh.b.id)
                    | None -> None

type private testWHFindCollectResource() =
    static member Test() =
        ResetTests()
        SetTestName("WHFindCollectResource")
        let whDef = {WHinfo<int,int>.Default1D with capacity=10}
        let i0 =    whDef   |> WHSetT Resource.Food 3 
                            |> WHAddInv Resource.Food 1 
                            |> WHAddInv Resource.RawFood 1
                            |> WHAddER Resource.Food 1
                            |> WHAddER Resource.RawFood 1
                            |> WHInfoSid 0
        let b0 = WH(i0)
        
        let i1 = i0 |> WHSetT Resource.Food 0
                    |> WHInfoSid 1 
        let b1 = WH(i1)
        let iodef = IOinfo<int,int>.Default1D
        let i2 = {iodef with oType=Some(Resource.RawFood); delSize=3; shopFront=5; oStored=9}
        let b2 = IO(i2).sID 2
        let i3 = {i2 with oType=Some(Resource.Food)}
        let b3 = IO(i3).sID 3
        let availMap = Map([(Resource.RawFood,Map([(2,9)]));(Resource.Food,Map([(3,9)]))])
        //Add available buildings to collect from, 1 food 1 rawfood
        let gs = {GameState<int,int>.Empty() with buildings= PV.ofSeq [|b0;b1;b2;b3|]; resourcesAvailable=availMap}
        let gs_noShop = gs |> SetGSBld 2 (IO({i2 with shopFront=7})) |> SetGSBld 3 (IO({i3 with shopFront=7}))
        let gs_noCap = gs |> SetGSBld 0 (WH({i0 with enRoute=8}))

        TestEq "Will fill target first" 
            (WHFindCollectResource i0 gs)
            (Some((Resource.Food,3,b3.id,b0.id)))

        TestEq "If not target, then will fill other resource"
            (WHFindCollectResource i1 gs)
            (Some((Resource.RawFood,3,b2.id,b1.id)))

        Test "Wont allow filling due to insufficient shop front" 
            ((WHFindCollectResource i1 gs_noShop) =
            None)

        Test "Wont allow filling due already enRoute"
            ((WHFindCollectResource i0 gs_noShop)
            = None)
        
        PrintTests()



type UnitTests =
    static member run() =
        testWHFindCollectResource.Test()
