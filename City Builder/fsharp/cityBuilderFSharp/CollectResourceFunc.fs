﻿module CollectResourceFunc

open Global.Tester
open Citizenns
open Buildingns
open GameState
open PointType
open Map2DBlockns
open ResourceNs
open Global.PV
open Taskns
open Buildingns.funcs

/// <summary>
/// Sets up a citizen getting a resource from one building and taking it to another
/// </summary>
/// <param name="cit">The citizen to fetch the resource</param>
/// <param name="res">The resource to collect</param>
/// <param name="n">The quantity to collect</param>
/// <param name="bldFrom">The building to take the goods from</param>
/// <param name="bldTo">The building to deliver the goods</param>
/// <param name="gs">The current gamestate</param>
let CollectResourceFunc (cit:Citizen<'P>) res n (bldFrom:Building<'L,'P>) (bldTo:Building<'L,'P>) gs = 
    
    match bldTo with
    | IO(x) -> if (x.enRoute + x.iStored + n) > x.iMax then failwith "Not enough capacity in to building"
    | Core(_) | Blder _ | NotBuilt _ -> ()
    | WH(i) -> if ((WHFSRes res i) < n) then failwith "Not enough capacity in warehouse"
    
    match gs.resourcesAvailable.TryFind(res) with
    | None -> failwith "Resource not available"
    | Some(inv) -> match inv.TryFind(bldFrom.id) with
                    | None -> failwith "From building doesnt have any of this resource available"
                    | Some(av) -> if av < n then failwith "From building has insufficient resource available"
    
    if (gs.citizens.Length <= cit.id || cit.id < 0) then failwith "Citizen ID doesnt exist"
    
    let newCits = gs.citizens |>  PV.update cit.id {cit with tasks=[  Task.MoveTo(bldFrom.location);
                                                                    Task.Collect({building=bldFrom.id;resource=res;quantity=n});
                                                                    Task.MoveTo(bldTo.location);
                                                                    Task.Deliver({building=bldTo.id;resource=res;quantity=n})]}
    let newAvailable = gs.resourcesAvailable.Add(res, gs.resourcesAvailable.[res].Add(bldFrom.id,gs.resourcesAvailable.[res].[bldFrom.id] - n))
    let newAssigned = match gs.resourcesAssigned.ContainsKey(res) with
                        | true -> gs.resourcesAssigned.Add(res, gs.resourcesAssigned.[res].Add(bldFrom.id,match gs.resourcesAssigned.[res].ContainsKey(bldFrom.id) with
                                                                                                            | true ->  gs.resourcesAssigned.[res].[bldFrom.id] + n
                                                                                                            | false -> n))
                        | false -> gs.resourcesAssigned.Add(res, Map([(bldFrom.id,n)]))
    let newBuildings = 
        match bldTo with
        | IO(x) -> gs.buildings |> PV.update bldTo.id (IO({x with enRoute= x.enRoute + n}))
        | WH(i) -> gs.buildings |> PV.update bldTo.id (WH(i |> WHAddER res n))
        | NotBuilt(i,bld) -> gs.buildings |> PV.update bldTo.id (NotBuilt(i |> NBAddER res n,bld))
        | Core(_) | Blder _ -> failwith "Cannot deliver resources to core or builder buildings"
    
    {gs with citizens= newCits; buildings= newBuildings; resourcesAvailable=newAvailable; resourcesAssigned=newAssigned}

type private testCollectResourceFunc() =
    static member Test() =
        ResetTests()
        SetTestName("CollectResourceFunc")
        
        let c0 = Citizen<Point>.Default({x=1.0;y=1.0})
        let c1 = {c0 with id=1}
        let c2 = {c1 with id=2;tasks=[Task.MoveTo({x=10.0;y=10.0})]}
        let c3 = {c0 with id=3}

        let b0 = Building.Default(Map2DBlock(0,1,1,0),{x=1.0;y=1.0})
        let i0 = match b0 with | IO(x) -> x | _ -> failwith "error in test data"
        let i1 = {i0 with b={i0.b with id=1; location={x=5.0;y=5.0}}}
        let b1 = IO(i1)
        let i2 = {i0 with b={i0.b with id=2; location={x=15.0;y=15.0}}; iMax=10; enRoute=2;iStored=3}
        let b2 = IO(i2)

        let gs = {GameState<Map2DBlock,Point>.Empty() with
                    citizens = PV.ofSeq  [|c0;c1;c2|]
                    buildings = PV.ofSeq [|b0;b1;b2|]
                    resourcesAvailable = Map([  (Resource.RawFood,   Map([(0,10);(1,4)]));
                                                (Resource.Food,      Map([(0,9);(1,6)]))])
                    resourcesAssigned = Map([   (Resource.RawFood,   Map([(0,2);(1,3)]));
                                                (Resource.Food,      Map([(0,4);(1,5)]))])
                    }

        
        try
            ignore(CollectResourceFunc c1 Resource.Food 6 b1 b2 gs)
            Test "If to building doesnt have capacity then an exception is thrown" false
        with
        | _ -> Test "If to building doesnt have capacity then an exception is thrown" true

        try
            ignore(CollectResourceFunc c1 Resource.RawFood 5 b1 b2 gs)
            Test "If from building doesnt have resource available then exception is thrown" false
        with
        | _ -> Test "If from building doesnt have resource available then exception is thrown" true

        try 
            ignore(CollectResourceFunc c1 Resource.Food 5 b2 b2 gs)
            Test "Exception thrown if building is not in resource available map" false
        with
        | _ -> Test "Exception thrown if building is not in resource available map" true

        try
            ignore(CollectResourceFunc c3 Resource.Food 5 b1 b2 gs)
            Test "If citizen ID doesnt exist in the gamestate then exception is thrown" false
        with
        | _ -> Test "If citizen ID doesnt exist in the gamestate then exception is thrown" true
        
        let gs2 = (CollectResourceFunc c1 Resource.Food 5 b1 b2 gs)
        let tasks = gs2.citizens.[1].tasks

        Test "Citizen task list is set to move to building, collect resource, move to to building, deposit resource"
            (
                tasks = [   Task.MoveTo(b1.location);
                            Task.Collect({building=b1.id;resource=Resource.Food;quantity=5});
                            Task.MoveTo(b2.location);
                            Task.Deliver({building=b2.id;resource=Resource.Food;quantity=5})]
            )

        let gs3 = (CollectResourceFunc c2 Resource.Food 5 b1 b2 gs)

        Test "Citizens task queue is overwritten entirely, clearing previous tasks" 
            (tasks = gs3.citizens.[2].tasks)
        
        Test "The resource is moved from avaialble to assigned in the from building" 
            (gs2.resourcesAssigned.[Resource.Food].[1] = 10
            && gs2.resourcesAvailable.[Resource.Food].[1] = 1)
        
        Test "Amount enroute in the building is increased by the correct amount" 
            ((match gs2.buildings.[2] with IO(x) -> x.enRoute | _ -> failwith "error in test data") = 7) 
        
        //Tested in BuildingTicFunc
//        Test "Exception if WH doesnt have enough capacity" false

//        Test "Sucessfully marked as enREoute for a WH" false

        PrintTests()



type UnitTests =
    static member run() =
        testCollectResourceFunc.Test()
