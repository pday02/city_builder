﻿module FindWorkFunc

open Global.Tester
open Global.Array
open Global.PV
open Global.General

open Citizenns
open GameState
open Buildingns
open Global.Array
open Buildingns.funcs


/// <summary>
/// Takes a citizen and a gamestate and finds them work.
/// If no opportunities exist, then they will be set to looking.
/// Throws an exception of the citizen already has work.
/// </summary>
let FindWorkFunc cid gs = 
    let cit = gs.citizens |> PV.tryNth cid |> ThrowIfNone "Citizen not found" 
    match cit.empStatus with
    | EmpStatus.Working(_) -> failwith "Citizen already assigned a workplace"
    | _ -> 
        match gs.buildings |> PV.tryFind (fun e -> match BldToEmpTry e with | Some(x) -> x.core.emps.Length<x.core.empMax && x.core.emps.Length<x.core.empCap | None -> false) with
            | Some(b) -> {gs with   citizens= gs.citizens 
                                                |> PV.tryUpdate  cit.id {cit with empStatus=EmpStatus.Working(b.id)} 
                                                |> ThrowIfNone "Citizen not found in citizens";
                                    buildings= gs.buildings
                                                |> PV.tryUpdate b.id ((BldToEmp b).sEmps (cit.id :: (BldToEmp b).emps) |> EmpToBld)
                                                |> ThrowIfNone "Building not found in Buildings";
                                    events = AssignedWork({cit with empStatus = EmpStatus.Working(b.id)}) :: gs.events}
            | None -> {gs with citizens= gs.citizens
                                            |> PV.tryUpdate cit.id {cit with empStatus=EmpStatus.Seeking}
                                            |> ThrowIfNone "Citizen not found in citizens"}
                                                                                           
type private testFindWorkFunc() =
    static member Test() =
        ResetTests()
        SetTestName("FindWorkFunc")
        let c_exc = {Citizen<int>.Default(0) with empStatus=EmpStatus.Working(1); id=0}
        let gs_empty_1: GameState<int,int> = {GameState<int,int>.Empty() with citizens = PV.ofSeq [|c_exc|]}
        try
            ignore (FindWorkFunc 0 gs_empty_1)
            Test "Exception thrown if input citizen already has work" false
        with
           | _ -> Test "Exception thrown if input citizen already has work" true
        
        let c_exc_2 = {c_exc with id=1; empStatus=EmpStatus.Unemployed}
        try
            ignore (FindWorkFunc 1 gs_empty_1)
            Test "Exception thrown if citizen id doesn't exist" false
        with
            | _ -> Test "Exception thrown if citizen id doesn't exist" true

        let c1 : Citizen<int> = {Citizen<int>.Default(0) with id=0; empStatus=EmpStatus.Unemployed; name="bob"}
        let gs = {GameState<int,int>.Empty() with citizens = PV.ofSeq [|c1|]}
        Test "Citizen set to looking if no buildings added" ((FindWorkFunc 0 gs) = {gs with citizens=PV.ofSeq [|{c1 with empStatus=EmpStatus.Seeking}|]})
        
        let gs2 = {gs with buildings= PV.ofSeq [|Building<int,int>.Default(1,1);Building<int,int>.Default(2,2)|]}
        Test "Citizen set to looking if buildings added but no workplace available" ((FindWorkFunc 0 gs2) = {gs2 with citizens=PV.ofSeq [|{c1 with empStatus=EmpStatus.Seeking}|]})
        
        let defInfo = match Building<int,int>.Default(2,2).sID(1) with | IO(x) -> x | _ -> failwith "error in test"
        let i1 = {defInfo with e = {defInfo.e with empMax=2; empCap=2}}
        let gs3 = {gs2 with buildings = PV.ofSeq [|Building<int,int>.Default(1,1);IO(i1)|]}
        let gs4 = FindWorkFunc 0 gs3
        Test "Citzen assigned job if a workplace available" (gs4.citizens = PV.ofSeq [|{c1 with empStatus=EmpStatus.Working(1)}|])
        Test "When work is assigned, it is logged in the event queue" (Contains (AssignedWork({c1 with empStatus=EmpStatus.Working(1)})) (List.toArray gs4.events))
        Test "When work is assigned, the workplace has the citizen stored as an employee" ( (BldToEmp gs4.buildings.[1]).core.emps = [0])
        let c2 = {c1 with id=1}
        let c3 = {c1 with id=2}
        let gs5 = FindWorkFunc 1 {gs4 with citizens=PV.ofSeq  [|c1;c2;c3|]} |> FindWorkFunc 2
        Test "When work is assigned, the workplace id is stored correctly as multiple citizens" ((BldToEmp gs5.buildings.[1]).core.emps = [1;0])
        Test "Citizen not assigned work when multiple workplaces all of which are full"         ((BldToEmp gs5.buildings.[1]).core.emps = [1;0] && gs5.citizens.[2].empStatus = EmpStatus.Seeking)

        let gs6 = FindWorkFunc 1 {gs4 with citizens=PV.ofSeq  [|c1;c2;c3|]; buildings = PV.ofSeq [|Building<int,int>.Default(1,1);IO({i1 with e={i1.e with empMax=3; empCap=2; emps=[0]}})|] } |> FindWorkFunc 2
        Test "Citizen not assigned if workplaces are not full but are all at thier cap" ((BldToEmp gs6.buildings.[1]).core.emps = [1;0] && gs6.citizens.[2].empStatus = EmpStatus.Seeking)

        PrintTests()



type UnitTests =
    static member run() =
        testFindWorkFunc.Test()
