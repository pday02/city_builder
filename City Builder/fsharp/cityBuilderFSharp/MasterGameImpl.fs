﻿namespace MasterGameImpl

open MasterGame
open AddCitizenFunc
open AddBuildingFunc
open Global.Tester
open Global.SaveAndLoader
open GameState
open Citizenns
open Loc2DBlockns
open Map2DBlockns
open CityMap
open GameSettings
open FindWorkFunc
open Buildingns
open PointType
open CitizenTicFunc
open Global.PV
open BuildingTicFunc
open ResourceNs
open CancelTaskFunc
open CapEmployeeFunc
open SwapEmployeesFunc
open Taskns
open GameState.funcs
open Buildingns.funcs
open Global.Serialise

type findCitWorkFuncType<'L,'P> = int -> GameState<'L,'P> -> GameState<'L,'P>
type cancelFuncType<'L,'P> = int -> GameState<'L,'P> -> GameState<'L,'P>

type MasterGameSettings<'L,'P when 'P : equality> =
    {
        fAddCit : Citizen<'P> -> GameState<'L,'P> -> GameState<'L,'P> //Adds a citizen to the gamestate
        fAddBld : NewEmpFindFType<'L,'P> -> Building<'L,'P> -> GameState<'L,'P> -> GameState<'L,'P> // Adds a building to the gamestate
        fFindCitsToWork : NewEmpFindFType<'L,'P>
        fFindWork : findCitWorkFuncType<'L,'P> //Finds work for a citizen
        fCitizenTic : CityMap<'L,'P> -> float -> GameState<'L,'P> -> Citizen<'P> -> GameState<'L,'P> //Runs every tic updating citizen stats
        fBuildingTic : float -> int -> GameState<'L,'P> -> GameState<'L,'P>
        fCancelTask : cancelFuncType<'L,'P>
        fBuildingEmpCap : cancelFuncType<'L,'P> -> findCitWorkFuncType<'L,'P> -> NewEmpFindFType<'L,'P> -> int -> int -> GameState<'L,'P> -> GameState<'L,'P>
        fSwapEmployees : cancelFuncType<'L,'P> -> int -> int -> int -> int -> GameState<'L,'P> -> GameState<'L,'P>
    }
    with
    static member Default() =
        {
            fAddCit = addCitizen
            fAddBld = addBuilding
            fFindWork = FindWorkFunc
            fCitizenTic = CitizenTicFunc (CitizenTicFunctions<'L,'P>.Default())
            fBuildingTic = BuildingTicFunc defaultBuildingTicFunctions 
            fCancelTask = CancelTaskFunc
            fFindCitsToWork = GetNewEmps
            fBuildingEmpCap = CapEmployeeFunc
            fSwapEmployees = SwapEmployeesFunc
        }

type MasterGameImpl<'L,'P when 'P : equality>(fs:MasterGameSettings<'L,'P>, cityMap:CityMap<'L,'P>) =
    new(map) = 
        MasterGameImpl(MasterGameSettings<'L,'P>.Default(), map)
    interface MasterGame<'L,'P> with
        member this.setWHTarget(id: int) (r: Resource) (n: int) (gs: GameState<'L,'P>): GameState<'L,'P> = 
            match gs |> GetGSBld id with
            | WH(i) -> gs |> SetGSBld id (WH(WHSetT r n i)) 
            | _ -> failwith "building id is not a warehouse"
        
        member this.addBuilding bld gs =
            if (not (cityMap.isBuildingValid bld.area gs)) then 
                (false, gs)
            else
                (true, fs.fAddBld fs.fFindCitsToWork bld gs)
            
        member this.addCitizen c gs = 
            match c.empStatus with
            | EmpStatus.Unemployed -> fs.fAddCit c gs
            | EmpStatus.Seeking -> fs.fAddCit c gs |> fs.fFindWork (gs.citizens.Length)
            | EmpStatus.Working(_) -> failwith "Added citizen is already working" 
        member this.clearEventQueue gs = {gs with events = List.empty} 
        member this.findWork cid gs = fs.fFindWork cid gs
        member this.tic dt gs = 
            //eff: This loops through every citizen every tick, some could likely be avoided
            let gs1 = gs.citizens |> PV.fold(fs.fCitizenTic cityMap dt) gs 
            
            gs.buildings |> PV.fold (fun gs b -> fs.fBuildingTic dt b.id gs) gs1
            //eff:this loops through every building every second, and could likely be avoided
        member this.empSwap b1 i1 b2 i2 gs = fs.fSwapEmployees fs.fCancelTask b1 i1 b2 i2 gs
        member this.empCap id n gs = fs.fBuildingEmpCap fs.fCancelTask fs.fFindWork fs.fFindCitsToWork id n gs
        member this.load s = 
            ((SaverAndLoader() :> Loader).LoadFromFile s (typeof<GameState<'L,'P>>)) :?> GameState<'L,'P>

type DefaultMasterImpl() = 
    static member MasterGame() =
        let r, t = (GameSettings.Default()).mapSize
        MasterGameImpl(Map2DBlock(0,r,t,0))
        
type private testMasterGameImpl() =
    static member Test() =
        ResetTests()
        SetTestName("MasterGameImpl")

        let mg = DefaultMasterImpl.MasterGame() :> MasterGame<Loc2DBlock,Point>

        //Test clear Event Queue
        let gs1 = GameState<Loc2DBlock,Point>.Empty()
        Test "Already empty queue stay empty" (mg.clearEventQueue gs1 = gs1)
        
        let gs2 = {GameState<Loc2DBlock,Point>.Empty() with events = [NewCitizen(Citizen<Point>.Default({x=0.0;y=0.0}))]; citizens= PV.ofSeq [|Citizen<Point>.Default({x=0.0;y=0.0})|]}
        let gs3 = mg.clearEventQueue gs2
        Test "Queue become empty if not empty" (gs3.events = List.empty)
        Test "Only queues become clear, nothing else" ({gs3 with events = [NewCitizen(Citizen<Point>.Default({x=0.0;y=0.0}))]} = gs2)
        
        //Test add building in a separate file, give it a map
        let c = {Citizen<Point>.Default({x=0.0;y=0.0}) with name = "Test"}
        let mockBldAdd a x y = {GameState<int,Point>.Empty() with citizens = PV.ofSeq [|c|]}
        let MockMG = MasterGameImpl({MasterGameSettings<Loc2DBlock,Point>.Default() with fAddBld=mockBldAdd},Map2DBlock(0,1,1,0)) :> MasterGame<Loc2DBlock,Point>
        Test "When adding building, checks that it is valid" ((MockMG.addBuilding (Building.Default({left=0;right=2;top=2;bottom=0},{x=0.0;y=0.0})) gs1) = (false,gs1))
        Test "When adding building, if valid then returns building added" ((MockMG.addBuilding (Building.Default({left=0;right=1;top=1;bottom=0},{x=0.0;y=0.0})) gs1) = (true,{GameState<int,Point>.Empty() with citizens = PV.ofSeq [|c|]}))

        let c_emp = {Citizen<Point>.Default({x=0.0;y=0.0}) with name="Emp"; empStatus=Working(2)}
        let c_seek :Citizen<Point>= {Citizen<Point>.Default({x=0.0;y=0.0}) with name="Seeking"; empStatus = Seeking; id=50}
        let mock_emp x y = {GameState<Loc2DBlock,Point>.Empty() with events=[NewCitizen({c_emp with id=x})]}
        let EmpMockMG = MasterGameImpl({MasterGameSettings<Loc2DBlock,Point>.Default() with fFindWork=mock_emp},Map2DBlock(0,1,1,0)) :> MasterGame<Loc2DBlock,Point>
        Test "When citizen added, if its status is seeking it will try to find a job" ((EmpMockMG.addCitizen c_seek gs1) = {GameState<Loc2DBlock,Point>.Empty() with events=[NewCitizen({c_emp with id=0})]})
        try
            ignore (EmpMockMG.addCitizen c_emp gs1)
            Test "When adding a citizen, if its job status is employed it will throw an exception" false
        with
        | _ -> Test "When adding a citizen, if its job status is employed it will throw an exception" true

        let c_move0 : Citizen<Point> = {Citizen<Point>.Default({x=1.0;y=1.0}) with id=0; tasks=[Task.MoveTo({x=10.0;y=1.0})]}
        let c_move1 : Citizen<Point> = {Citizen<Point>.Default({x=2.0;y=1.0}) with id=1; tasks=[Task.MoveTo({x=10.0;y=1.0})]}
        let gs_cit_tic = {GameState<Loc2DBlock,Point>.Empty() with citizens=PV.ofSeq [|c_move0;c_move1|]}
        Test "Every citizen is run through the citizen tic function every tic" ((MockMG.tic 0.5 gs_cit_tic) = {gs_cit_tic with citizens=PV.ofSeq [|{c_move0 with location={x=1.5;y=1.0}}; {c_move1 with location={x=2.5;y=1.0}}|]})

        let c_prod0 : Citizen<Point> = {Citizen<Point>.Default({x=1.0;y=1.0}) with id=0; tasks=[Task.Idle]; empStatus=Working(0)}
        let c_prod1 : Citizen<Point> = {Citizen<Point>.Default({x=1.0;y=1.0}) with id=1; tasks=[Task.Idle]; empStatus=Working(1)}

        let tempb = Building.Default({top=0;bottom=0;left=0;right=0}, {y=1.0;x=1.0}).sID(0)
        let itemp = match tempb with IO(x) -> x | _ -> failwith "test data not available"

        let i0 = {itemp with iType=None; oMax=10; e={itemp.e with emps=[0]; empMax=10}; oType=Some(Resource.Food); timeToProduce=15.0; oStored=0}
        let b0 = IO(i0)
        let i1 = {i0 with b={i0.b with id=1}; e= {i0.e with emps=[1]}}
        let b1 = IO(i1)
        let gs_buildTic = {gs_cit_tic with citizens=PV.ofSeq [c_prod0;c_prod1]; buildings=PV.ofSeq [b0;b1]}
        let gs_buildTic2 = MockMG.tic 0.5 gs_buildTic
        Test "Every building is run through the building tic function every tic" 
            (match gs_buildTic2.citizens.[0].tasks with | Task.Produce(_) :: _ -> true | _ -> false 
            && match gs_buildTic2.citizens.[1].tasks with | Task.Produce(_) :: _ -> true | _ -> false)

        PrintTests()

type UnitTests =
    static member run() =
        testMasterGameImpl.Test()