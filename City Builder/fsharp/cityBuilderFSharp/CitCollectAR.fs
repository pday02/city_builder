﻿module CitCollectAR

open Global.Tester
open GameState
open Citizenns
open Buildingns
open Taskns
open ResourceNs
open Global.PV
open Global.General
open Global.Map
open GameState.funcs
/// <summary>
/// Run when a citizen task is to collect an abandoned resource
/// </summary>
/// <param name="x"></param>
let CitCollectAR (cid:int) (gs:GameState<'L,'P>) = 
    let c = gs.citizens |> PV.tryNth cid |> ThrowIfNone "Citizen id not found"
    match c.tasks with
    | CollectAR(ar) :: t ->
        if (ar.loc <> c.location) then
            gs |> SetGSCit cid {c with tasks=MoveTo(ar.loc) :: c.tasks}
        else
            gs  |> SetGSCit cid {c with resCarried = c.resCarried |> AddToMapFunc (+) (fun x -> x) ar.rType ar.quantity
                                        tasks = if (t.IsEmpty) then [Idle] else t}
                |> AddEvent (ARCollected(ar))
    | _ -> failwith "CollectAR task not at front of queue"
                

type private testCitCollectAR() =
    static member Test() =
        ResetTests()
        SetTestName("CitCollectAR")
        
        let c0 = {Citizen<int>.Default1D with id=0; location=5; tasks=[Task.CollectAR({quantity=3;loc=7;rType=Resource.Food;destID=2})]}
        let c1 = {c0 with location=7; id=1; resCarried=Map([(Resource.Food,5);(Resource.RawFood,6)]); tasks=[Task.CollectAR({quantity=3;loc=7;rType=Resource.Food;destID=2});Task.MoveTo(-10)]}
        let c2 = {c1 with id=2; tasks=c1.tasks.Tail}
        let b0 = Building<int,int>.Default1D.sID(3)

        let gs = {GameState<int,int>.Empty() with citizens=PV.ofSeq [c0;c1;c2]; buildings=PV.ofSeq [b0]}

        Test "If citizen isn't at the pickup location then it will be sent to the pickup location" 
            ((CitCollectAR 0 gs).citizens.[0].tasks = [Task.MoveTo(7);Task.CollectAR({quantity=3;loc=7;rType=Resource.Food;destID=2})])

        let gs2 = CitCollectAR 1 gs
        Test "Resource put in inventory, event raised, and task removed from front for citizen"
            ((gs2.citizens.[1].resCarried = Map([(Resource.Food,8);(Resource.RawFood,6)])) &&
                gs2.events = [ARCollected({quantity=3;loc=7;rType=Resource.Food;destID=2})] &&
                gs2.citizens.[1].tasks = c1.tasks.Tail
            )

        try
            ignore(CitCollectAR 2 gs)
            Test "Exception throw if CollectAR not at front of the list" false
        with
        | _ -> Test "Exception throw if CollectAR not at front of the list" true

        PrintTests()



type UnitTests =
    static member run() =
        testCitCollectAR.Test()
