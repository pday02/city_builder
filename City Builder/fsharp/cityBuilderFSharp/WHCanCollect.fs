﻿module WHCanCollect

open GameState
open GameState.funcs
open Buildingns
open Global.Map

/// <summary>
/// Returns whether this warehouse has something it can collect or not
/// </summary>
/// <param name="bid"></param>
/// <param name="gs"></param>
// Can be collected if there is a batch avilable above the shop front
    //and the amount between current and max is sufficient in the warehouse
let WHCanCollect bid (gs:GameState<'a,'b>) =
    let b = gs |> GetGSBld bid
    match b with
    | WH(i) -> 
       false
       // if (i.curStored >= i.maxStored) then false
       // else
       //     let m = gs.resourcesAvailable |> Map.toArray |> Array.map(fun (x,y) -> x)
       //     let whAllocated = MapMap2 (max) 0 (i.targetStorage) i.inv |> Map.fold(fun s k x -> s + x) 0
           // let resPoss r = (max 0 ((MapFindDef r 0 i.targetStorage) - MapFindDef r 0 i.inv)), //1st term in tuple poss storage within target
                            
            //check how much of a resource can be acquired
            //reosurce can be sought if its within target and 
            //setting target will reserve the good
       //     false
    | IO(_) | Core(_) | NotBuilt _ | Blder _ -> failwith "Function shoudljt be applied to IO or cores"
