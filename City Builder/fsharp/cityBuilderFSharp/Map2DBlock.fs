﻿namespace Map2DBlockns

open Global.Tester
open CityMap
open Loc2DBlockns
open Buildingns
open Global.Array
open PointType
open Citizenns
open GameState
open System
open Global.PV
open Taskns

///A 2D block map (i.e. a grid). The left, right, top, bottom values draws the map 
type Map2DBlock(left, right, top, bottom) =
    let validShape l r t b = (l < r) && (b < t)
    let noOverlap l1 l2 =
        (l1.left >= l2.right) || (l1.right <= l2.left) || l1.top <= l2.bottom || l1.bottom >= l2.top
    do
        if not (validShape left right top bottom) then failwith "Illegal values in constructor"  
    interface CityMap<Loc2DBlock,Point> with
        member this.isBuildingValid loc gs = 
            let loc_l, loc_r, loc_t, loc_b = loc.left, loc.right, loc.top, loc.bottom
            if not (validShape loc_l loc_r loc_t loc_b) then failwith "Illegal shape passed for checking valid location"
            if ((loc_l < left) || (loc_b < bottom) || (loc_t > top) || (loc_r > right)) then
                false
            else
                ///eff: This loops through all building checking for overlap. WOuld be much more efficient to have buildings register a local region they are in, and only search those that are in a region that would be affected by the new building.
                gs.buildings |> PV.map(fun x -> x.area) |> PV.trueForAll (noOverlap loc) 
        member this.moveCit dt c gs = 
            //eff: This calculates the direction every tic. Passing a function with it stored would be quicker.
            
            if dt < 0.0 then failwith "Time interval is negative"
            match c.tasks.Head with
            | Task.MoveTo(p) ->
                if (p = c.location) then p
                else
                    let Sign x y = if x > y then 1.0 else -1.0
                    let angle = Math.Atan(Math.Abs(c.location.y - p.y)/Math.Abs(c.location.x - p.x))
                    let yMag = Math.Sin(angle) * dt
                    let yDir = Sign p.y c.location.y
                    let xMag = Math.Cos(angle) * dt 
                    let xDir = Sign p.x c.location.x
                    let newX = if (Sign p.x (c.location.x + (xMag * xDir)) = xDir) then (c.location.x + (xMag * xDir)) else p.x 
                    let newY = if (Sign p.y (c.location.y + (yMag * yDir)) = yDir) then (c.location.y + (yMag * yDir)) else p.y
                    {x= newX; y= newY}
            | _ -> failwith "First task is not a move to task"  

type private testMap2DBlock() =
    static member Test() =
        ResetTests()
        SetTestName("Map2DBlock object")
        
        //Creation
        try
            ignore(Map2DBlock(10, 10, 10, 0))
            Test "If left = right then exception thrown in constructor" false
        with
            | _ -> Test "If left = right then exception thrown in constructor" true

        try 
            ignore(Map2DBlock(10,9,10,0))
            Test "If left > right then exception thrown in constructor" false
        with
            | _ -> Test "If left > right then exception thrown in constructor" true

        try
            ignore(Map2DBlock(0,10,5,5))
            Test "If bottom = top then exception thrown in constructor" false
        with
            | _ -> Test "If bottom = top then exception thrown in constructor" true 

        try
            ignore(Map2DBlock(0,10,5,6))
            Test "If bottom > top then exception thrown in constructor" false
        with
            | _ -> Test "If bottom > top then exception thrown in constructor" true


        //isBuildingValid
        let map = Map2DBlock(-10,10,10,-10) :> CityMap<Loc2DBlock,Point>
        let empty_gs = GameState.GameState<Loc2DBlock,Point>.Empty()
        let b1 = Building<Loc2DBlock,Point>.Default({left= -2;right=2;top=8;bottom=6},{x=0.0;y=0.0})
        let b2 = Building<Loc2DBlock,Point>.Default({left= -2;right=2;top= -6;bottom= -8},{x=0.0;y=0.0})
        let b3 = Building<Loc2DBlock,Point>.Default({left= -8;right= -6;top=2;bottom= -2},{x=0.0;y=0.0})
        let b4 = Building<Loc2DBlock,Point>.Default({left= 6;right=8;top=2;bottom= -2},{x=0.0;y=0.0})
        let gs_b = {empty_gs with buildings=PV.ofSeq [|b1;b2;b3;b4|]}
        Test "Returns false if left is outside map" (not (map.isBuildingValid ({left= -11;right=9;top=9;bottom= -9}) empty_gs))
        Test "Returns true if left is on line" (map.isBuildingValid ({left= -10;right=9;top=9;bottom= -9}) empty_gs)
        Test "Returns false if bottom is outside map" (not (map.isBuildingValid ({left= -9;right=9;top=9;bottom= -19}) empty_gs))
        Test "Returns true if bottom is on line" (map.isBuildingValid ({left= -1;right=9;top=9;bottom= -10}) empty_gs)
        Test "Returns false if top is outside map" (not (map.isBuildingValid ({left= -9;right=9;top=19;bottom= -9}) empty_gs))
        Test "Returns true if top is on line" (map.isBuildingValid ({left= -9;right=9;top=10;bottom= -9}) empty_gs)
        Test "Returns false if right is outside map" (not (map.isBuildingValid ({left= -9;right=19;top=9;bottom= -9}) empty_gs))
        Test "Returns true if right is on line" (map.isBuildingValid ({left= -9;right=10;top=9;bottom= -9}) empty_gs)
        Test "Returns true if all inside and no other buildings" (map.isBuildingValid ({left= -9;right=9;top=9;bottom= -9}) empty_gs)
        Test "Returns true of all inside and away from other buildings" (map.isBuildingValid ({left= -1;right=1;top=1;bottom= -1}) gs_b)
        Test "Returns true if on boundaries on left other buildings" (map.isBuildingValid {left= -6;right=1;top=1;bottom= -1} gs_b)
        Test "Returns true if on boundaries on right other buildings" (map.isBuildingValid {left= -1;right=6;top=1;bottom= -1} gs_b)
        Test "Returns true if on boundaries on top other buildings" (map.isBuildingValid {left= -1;right=1;top=6;bottom= -1} gs_b)
        Test "Returns true if on boundaries on bottom other buildings" (map.isBuildingValid {left= -1;right=1;top=1;bottom= -6} gs_b)
        Test "Returns false if overlaps with another building on left" (not (map.isBuildingValid {left= -7;right=1;top=1;bottom= -1} gs_b))
        Test "Returns false if overlaps with another building on right" (not (map.isBuildingValid {left= -1;right=7;top=1;bottom= -1} gs_b))
        Test "Returns false if overlaps with another building on top" (not (map.isBuildingValid {left= -1;right=1;top=7;bottom= -1} gs_b))
        Test "Returns false if overlaps with another building on bottom" (not (map.isBuildingValid {left= -1;right=1;top=1;bottom= -7} gs_b)) 
        try
            ignore(map.isBuildingValid  {left= 1;right=1;top=1;bottom= -1} empty_gs)
            Test "If left = right on input location when checking location is valid then exception thrown" false
        with
            | _ -> Test "If left = right on input location when checking location is valid then exception thrown" true

        try 
            ignore(map.isBuildingValid  {left= 2;right=1;top=1;bottom= -1} empty_gs)
            Test "If left > right on input location when checking location is valid then exception thrown" false
        with
            | _ -> Test "If left > right on input location when checking location is valid then exception thrown" true

        try
            ignore(map.isBuildingValid  {left= -1;right=1;top=1;bottom= 1} empty_gs)
            Test "If top = bottom on input location when checking location is valid then exception thrown" false
        with
            | _ -> Test "If top = bottom on input location when checking location is valid then exception thrown" true 

        try
            ignore(map.isBuildingValid  {left= -1;right=1;top= -2;bottom= -1} empty_gs)
            Test "If top < bottom on input location when checking location is valid then exception thrown" false
        with
            | _ -> Test "If top < bottom on input location when checking location is valid then exception thrown" true


        let c = Citizen<Point>.Default({x=0.1;y=0.1})
        let gs:GameState<Loc2DBlock,Point> = GameState<Loc2DBlock,Point>.Empty()
        //Move towards
        try
            ignore (map.moveCit -0.1 c gs)
            Test "If time negative then exception thrown" false
        with
        | _ -> Test "If time negative then exception thrown" true

        
        let c2 = {Citizen<Point>.Default({x=2.0;y=2.0}) with tasks=[Task.MoveTo({x=2.0;y=2.0})] }
        Test "If citizen is already at location the final location is returned" ((map.moveCit 10.0 c2 gs) = {x=2.0;y=2.0})
        let c3 = {c2 with location = {x=0.5;y=0.5};tasks=[Task.MoveTo({x=1.5;y=1.5})]}
        Test "The citizen will stop at the point" ((map.moveCit 10.0 c3 gs) = {x=1.5;y=1.5})
        
        let c4 = {c3 with tasks=[Task.MoveTo({x= -29.5;y=40.5})]}
        Test "Direction correct with negative x and positive y" ((map.moveCit 5.0 c4 gs) = {x= -2.5;y=4.5})
        let c5 = {c3 with tasks=[Task.MoveTo({x= 30.5;y=40.5})]}
        Test "Direction correct with positive x and positive y" ((map.moveCit 5.0 c5 gs) = {x=3.5;y=4.5})
        let c6 = {c3 with tasks=[Task.MoveTo({x= -29.5;y= -39.5})]}
        Test "Direction correct with negative x and negative y" ((map.moveCit 5.0 c6 gs) = {x= -2.5;y= -3.5})
        let c7 = {c3 with tasks=[Task.MoveTo({x= 30.5;y= -39.5})]}
        Test "Direction correct with positive x and negative y" ((map.moveCit 5.0 c7 gs) = {x= 3.5;y= -3.5})
        
        let c8 = {c7 with tasks=[]}
        try
            ignore (map.moveCit 5.0 c8 gs)
            Test "Exception thrown if citizens first task isn't MoveTo" false
        with
        | _ -> Test "Exception thrown if citizens first task isn't MoveTo" true


        PrintTests()


type UnitTests =
    static member run() =
        testMap2DBlock.Test()
