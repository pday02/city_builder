﻿module CitizenTicFunc

open Global.Tester
open Citizenns
open PointType
open GameState
open Loc2DBlockns
open Map2DBlockns
open CityMap
open Taskns
open Global.PV
open Global.General
open CitPickUpResFunc
open CitDepositResFunc
open CitProduceFunc
open Buildingns
open ResourceNs
open CitCollectAR
open CitConstructFunc

///todo: Add warehouse
///todo: Citizens get hungry and will stop what they are doing to gather and eat Resource.Food
///todo: separate lists for different building types. There will be a lot of houses!
///todo: move into citizens fulfilling thier demands at certain times, whether it be shifts, working hours etc.
///todo: Goods stay on display in shops
///todo: Citizens have work patterns, early 6-3, late 9-6. Could also do lunchtimes aswell. This gives everyone time to shop
///todo: 5am people get up, 11pm people sleep. Daily schedules with breakfast, lunch, entertainment etc.
///todo: create triggers/listeners rather than cycling through every citizen and worplace every tic.

///todo: Create a functional timed queue. Based on a PV with ID as index, 
///     each element is the id before it, id after it, and time for this. A pointer can be kept at the front, 
///     meaning that for things that take a certain time to do (e.g. hunger timed counters) only the front ones needs to be checked
///     trackers can be added to make insertions at commin times too, updating at tic to show where a new insertion would be made
///     This would be useful for finishing shifts, working produce etc.  

///todo: State saving to file so errors can be found, gameplay can be repeated etc. Players actions saved when exception hit (or when player hits a button to save it)
///todo: when as exception hits, have it automatically save as test that runs in run tests, saying passed if no exception hit or fails if it does.

/// <summary>
/// Updates the gamestate given time passing for a citizen
/// </summary>
/// <param name="dt">The chagne in time</param>
/// <param name="gs">The input gamestate</param>
/// <param name="cit">The citizen to update it for</param>

type CitizenTicFunctions<'L,'P when 'P : equality> =
    {
        fIdle : Citizen<'P> -> GameState<'L,'P> -> GameState<'L,'P>
        fNewTask : Citizen<'P> -> GameState<'L,'P> -> GameState<'L,'P>
        fCollectResource : Citizen<'P> -> GameState<'L,'P> -> GameState<'L,'P>
        fDepositResource : Citizen<'P> -> GameState<'L,'P> -> GameState<'L,'P>
        fProduceGood : float -> int -> GameState<'L,'P> -> GameState<'L,'P>
        fConstructBld : float -> int -> int -> GameState<'L,'P> -> GameState<'L,'P>
        fCollectAR : int -> GameState<'L,'P> -> GameState<'L,'P>
    }
    with
    static member Default() =  
        {
            ///A function that is run every tic on a citizen that is idle
            fIdle = fun c gs -> gs
            //A function that is run when a citzen finishes the last task
            fNewTask = fun c gs -> gs
            //A function that transfers resources from a building to a citizen
            fCollectResource = CitPickUpResFunc
            //A function that transfers resources from a citizen to a building
            fDepositResource = CitDepositResFunc 
            //A function that produces a good
            fProduceGood = CitProduceFunc
            //A fucntion that transfers an abandoned resource to the citizens inventory
            fCollectAR = CitCollectAR
            fConstructBld = CitConstructFunc
        }

let rec CitizenTicFunc<'L,'P when 'P : equality>  fs (map:CityMap<'L,'P>) dt gs (inCit:Citizen<'P>) =
    let cit = ThrowIfNone "Citizen ID not found in citizens" (PV.tryNth inCit.id gs.citizens)  //Assigning the citizen from the gs incase anything had changed
    match cit.tasks with
    | [] -> fs.fIdle {cit with tasks=[Task.Idle]} {gs with citizens= PV.update cit.id {cit with tasks=[Task.Idle]} gs.citizens}
    | _ -> match cit.tasks.Head with
            | Task.MoveTo(p) -> 
                let newCit = {cit with location = map.moveCit dt cit gs}
                if (newCit.location = p) then
                    let newEvents = TaskComplete(cit,cit.tasks.Head) :: gs.events 
                    if (newCit.tasks.Tail.IsEmpty) then
                        fs.fNewTask {newCit with tasks=[]} {gs with citizens=PV.update cit.id {newCit with tasks=[]} gs.citizens; events=newEvents}
                    else
                        {gs with citizens=PV.update cit.id {newCit with tasks=newCit.tasks.Tail} gs.citizens; events=newEvents}
                else
                    {gs with citizens=PV.update cit.id newCit gs.citizens}
            | Task.Idle -> fs.fIdle cit gs
            | Task.Collect(_) -> fs.fCollectResource cit gs
            | Task.Deliver(_) -> fs.fDepositResource cit gs
            | Task.Produce(_) -> fs.fProduceGood dt cit.id gs
            | Task.CollectAR(_) -> fs.fCollectAR cit.id gs
            | Task.Build n -> fs.fConstructBld dt cit.id n gs


type private testCitizenTicFunc() =
    static member Test() =
        ResetTests()
        SetTestName("CitizenTicFunc")

        //Test MoveTo
        let c1 = {Citizen<Point>.Default({x=1.0;y=1.0}) with id=0; tasks=[Task.MoveTo({x=2.0;y=1.0})]}
        let c2 = {Citizen<Point>.Default({x=1.0;y=1.0}) with id=1; tasks=[Task.Idle]}
        let c3 = {Citizen<Point>.Default({x=1.0;y=1.0}) with id=2; tasks=[Task.MoveTo({x=2.0;y=1.0});Task.MoveTo({x=10.0;y=10.0});Task.Idle]}
        let c4 = {c2 with tasks=[]; id=3}
        let gs:GameState<Loc2DBlock,Point> = {GameState<Loc2DBlock,Point>.Empty() with citizens = PV.ofSeq [|c1;c2;c3;c4|]}
        let m = Map2DBlock(-30,30,30,-30) :> CityMap<Loc2DBlock,Point>
        let fs = {  fIdle = (fun c gs -> {gs with citizens = PV.ofSeq [|c|]}); 
                    fNewTask = (fun c gs -> {gs with citizens = PV.ofSeq [|c;c|]});
                    fCollectResource = (fun c gs -> {gs with events=[NewCitizen(c)]}); 
                    fDepositResource = (fun c gs -> {gs with events=[NewCitizen(c);NewCitizen(c)]});
                    fProduceGood = (fun dt i gs -> {gs with events=[NewCitizen({gs.citizens.[i] with tasks = [Produce({cit=i;building=0;resource=Resource.Food;timeLeft=dt})]})]})
                    fCollectAR = CitCollectAR
                    fConstructBld = CitConstructFunc}
        let f = CitizenTicFunc fs m
        Test "If citizen is moving then the move function is called with the new location set" ((f 0.5 gs c1) = {gs with citizens=PV.ofSeq [|{c1 with location={x=1.5;y=1.0}};c2;c3;c4|]})
        Test "Movement made when citizen is later in the array" ((f 0.5 gs c3) = {gs with citizens=PV.ofSeq [|c1;c2;{c3 with location={x=1.5;y=1.0}};c4|]})
        Test "If a citizen has reached its location, an event is raised" ((f 1.5 gs c1).events = [TaskComplete((c1,c1.tasks.Head))])
        Test "If citizen reaches it location, the next task is set" ((f 20.5 gs c3).citizens = PV.ofSeq  [|c1;c2;{c3 with location={x=2.0;y=1.0}; tasks=c3.tasks.Tail};c4|])
        Test "Idle citizens have the idle function called on them" ((f 20.0 gs c2).citizens = PV.ofSeq [|c2|])
        Test "If the citizen reaches its location and thre isn't another task, the getTask function is called" ((f 20.0 gs c1).citizens = PV.ofSeq [|{c1 with location={x=2.0;y=1.0};tasks=[]};{c1 with location={x=2.0;y=1.0};tasks=[]}|])
        Test "If a task list is empty, it is made idle and the idle function is run" ((f 20.0 gs c4).citizens = PV.ofSeq [|{c2 with id=3}|]) 
        
        let c_col = {c2 with tasks=[Task.Collect({quantity=1;building=Building.Default({left=0;right=0;top=0;bottom=0},{y=0.0;x=0.0}).id;resource=Resource.Food})]}
        let gs_col = {gs with citizens = gs.citizens|> PV.update c_col.id c_col} 
        Test "Collect func run when citzien has collect as its task" ((f 0.5 gs_col c_col) = {gs_col with events=[NewCitizen(c_col)]})

        let c_dep = {c2 with tasks=[Task.Deliver({quantity=1;building=Building.Default({left=0;right=0;top=0;bottom=0},{y=0.0;x=0.0}).id;resource=Resource.Food})]}
        let gs_dep = {gs with citizens = gs.citizens|> PV.update c_dep.id c_dep} 
        Test "Deposit func run when citizen has deposit as its task" ((f 0.5 gs_dep c_dep) = {gs_dep with events=[NewCitizen(c_dep);NewCitizen(c_dep)]})

        let c_prod = {c_dep with id=0;tasks=[Produce({cit=10;building=0;resource=Resource.Food;timeLeft=0.0})]}
        let gs_prod_in = {gs with  citizens = gs.citizens|> PV.update c_prod.id c_prod}
        let gs_prod_out = {gs_prod_in with events=[NewCitizen({gs.citizens.[0] with tasks = [Produce({cit=0;building=0;resource=Resource.Food;timeLeft=0.5})]})]}
        Test "Produce func called correctly when produce is its task" ((f 0.5 gs_prod_in c_prod)=gs_prod_out)

        PrintTests()


type UnitTests =
    static member run() =
        testCitizenTicFunc.Test()
