﻿module ProduceResourceFunc

open Global.Tester
open Citizenns
open PointType
open Buildingns
open Loc2DBlockns
open GameState
open Global.PV
open Taskns
open Global.General
open ResourceNs
/// <summary>
/// Makes the citizen start producing a good. The function
/// does not check that the building has the capacity to do so, it is
/// assumes this is checked beforehand. Same for that the citizen is idle.
/// </summary>
/// <param name="citID">The citizen that is going to start producing</param>
let ProduceResourceFunc citID (gs:GameState<'L,'P>) = 
    let c = gs.getCit citID
    match c.empStatus with
    | EmpStatus.Working(bId) ->
        let b = gs.getBld bId
        match b with
        | IO(i) ->
            {gs with 
                citizens= gs.citizens |> PV.update citID {c with tasks=[Produce({building=b.id;cit=c.id;resource=i.oType |> ThrowIfNone "Resource type not set";timeLeft= i.timeToProduce})]}
                buildings= gs.buildings |> PV.update b.id (IO({i with iStored= (match i.iType with 
                                                                                | Some(_) -> i.iStored - 1 
                                                                                | None -> i.iStored)
                                                                                ; inProgress= i.inProgress + 1}))}
        | WH(_) | Core _ | Blder _ | NotBuilt _ -> failwith "Warehouses, builders, not built and cores do not prodcue goods"
    | _ -> failwith "Citizen is not working"

type private testProduceResourceFunc() =
    static member Test() =
        ResetTests()
        SetTestName("ProduceResourceFunc")
        
        let c0 = {Citizen<Point>.Default({x=0.0;y=0.0}) with empStatus=EmpStatus.Unemployed; id=0; tasks=[Task.Idle]}
        let c1 = {c0 with id=1; empStatus=EmpStatus.Working(1)}

        let b0 = Building.Default({top=0;bottom=0;left=0;right=0},{x=1.0;y=1.0}).sID(0)
        let i0 = match b0 with IO(x) -> x | _ -> failwith "Test data error"
        let i1 = {i0 with b={i0.b with id=1}; inProgress=4;iStored=2;oType=Some(Resource.Food);timeToProduce=3.5;iType=Some(Resource.Food)}
        let b1 = IO(i1)

        let gs = {GameState<int,int>.Empty() with citizens=PV.ofSeq [c0;c1]; buildings=PV.ofSeq [b0;b1]}
        
        try
            ignore(ProduceResourceFunc 3 gs)
            Test "Exception thrown if citizen not found" false
        with
        | _ -> Test "Exception thrown if citizen not found" true
        
        try
            ignore(ProduceResourceFunc 0 gs)
            Test "Exception thrown if citizen not working" false
        with
        | _ -> Test "Exception thrown if citizen not working" true

        let gs2 = ProduceResourceFunc 1 gs
        let gs2_cit = gs2.citizens.[1]
        let gs2_b = match gs2.buildings.[1] with | IO(x) -> x | _ -> failwith "error in data"
        Test "Removes 1 from input of the building producing" (gs2_b.iStored=1)
        Test "Puts 1 in progress of the building producing" (gs2_b.inProgress=5)
        Test "Puts produce task at the front of the citizens task queue" (gs2_cit.tasks.Head = Task.Produce({building=1;cit=1;resource=Resource.Food;timeLeft=3.5}))
        
        PrintTests()



type UnitTests =
    static member run() =
        testProduceResourceFunc.Test()
