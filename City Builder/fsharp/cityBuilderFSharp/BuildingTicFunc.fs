﻿module BuildingTicFunc

open Global.Tester
open Citizenns
open Buildingns
open GameState
open ResourceNs
open Loc2DBlockns
open PointType
open Global.PV
open CollectResourceFunc
open ProduceResourceFunc
open Global.General
open Taskns
open GameState.funcs
open Buildingns.funcs
open WHFindCollectResource
open AddBuildingFunc
open CancelTaskFunc

type BuildingTicFunctions<'L,'P> = 
    {
        ///Sets up a citizen to collect a resource that if available from another building
        /// cit, resource, quantity, from, to, gamestate
        fCollectResources: Citizen<'P> -> Resource -> int -> Building<'L,'P> -> Building<'L,'P> -> GameState<'L,'P> -> GameState<'L,'P>
        fProduce: int -> GameState<'L,'P> -> GameState<'L,'P> 
        fFindCollectPoint : Resource -> int -> GameState<'L,'P> -> int option
        fWHFindCollectPoint : WHinfo<'L,'P> -> GameState<'L,'P> -> (Resource*int*int*int) option 
        fCitCancelTask : int ->  GameState<'L,'P> ->  GameState<'L,'P>
    }

let defaultBuildingTicFunctions =
    {
        fCollectResources = CollectResourceFunc;
        fProduce = ProduceResourceFunc
        ///Finding a collect point is just taking the first that has the resource, regardless of where it is
        fFindCollectPoint = fun r i gs -> gs.resourcesAvailable.[r] |> Map.tryFindKey (fun k n -> n > 0)
        fWHFindCollectPoint = WHFindCollectResource
        fCitCancelTask = CancelTaskFunc
    }

/// <summary>
/// Runs every tic for each producer
/// </summary>
/// <param name="x"></param>
///eff: Better to not have something that has to tic for every building, use listeners etc
let rec BuildingTicFunc fs dt bldId gs = 
    let b = gs.buildings |> PV.tryNth bldId |> ThrowIfNone "Building ID not found"
    
    let highPriority =
        match b with
        | IO(i) -> not (i.priorityTasks.IsEmpty)
        | WH(i) -> not (i.priorityTasks.IsEmpty)
        | NotBuilt _ -> false
        | Blder i -> not (i.priorityTasks.IsEmpty)
        | Core(_) -> false
    
    let canProduce = 
        match b with 
        | IO(i) -> (i.iType.IsNone || i.iStored > 0) && (i.oMax > (i.inProgress + i.oStored))
        | Blder i -> false
        | Core(_) | WH(_) | NotBuilt _ -> false 


    let canCollect() = 
        match b with
        | IO(i) -> 
            if (i.iType.IsSome && ((i.iStored + i.enRoute) < i.iMax) && gs.resourcesAvailable.ContainsKey(i.iType.Value))
            then 
                match fs.fFindCollectPoint i.iType.Value b.id gs with
                | Some(n) -> Some(i.iType.Value,1,n,b.id )
                | None -> None
            else None
        | WH(i) -> 
            fs.fWHFindCollectPoint i gs
        | Core(_) -> None
        | NotBuilt _ -> None
        | Blder i -> 
            match i.blding with
                | None -> None
                | Some n -> match GetGSBld n gs with
                            | NotBuilt (nbi, nbBld) -> 
                                match (nbi.res |>  Map.tryPick(fun k inv -> if ((inv.req - inv.enR - inv.acq > 0) && ((GetGSAvail k gs) |> Map.exists (fun ki kn -> kn > 0))) 
                                                                                then Some(k) else None)) with
                                | Some(k) -> match gs |> GetGSAvail k |> Map.tryPick(fun ki ni -> if (ni>0) then Some(ki) else None) with
                                                    | Some(from) -> Some(k,1,from,n)
                                                    | None -> None
                                | None -> None
                            | _ -> None
                                                                                 

    let isAssignable (t: Task<'a> list) = (t.IsEmpty || (match t.Head with | Idle -> true | _ -> false))
    
    let nextIdle() = 
        match b.iEmp with
        | Some(e) -> e.emps |> List.tryFind(fun id -> gs.citizens.[id].tasks |> isAssignable)
        | None -> None
    
    ///todo: Move assigning what to do when taking on a high priority task into its own function for clenliness
    if highPriority then
        match b with
        | IO(i) -> 
            match i.priorityTasks with
            | PriorityTask.CollectAR(ar) :: t ->
                match nextIdle() with
                | Some(n) -> gs |> SetGSCitf n (fun c -> {c with tasks=[Task.CollectAR(ar);Task.Deliver({quantity=ar.quantity;resource=ar.rType;building=bldId})]})
                                |> SetGSBld bldId (IO({i with priorityTasks=t}))
                                |> BuildingTicFunc fs dt bldId
                | None -> gs
            | [] -> failwith "IO must have priority tasks to be assigned"
        | WH(i) -> 
            match i.priorityTasks with
            | PriorityTask.CollectAR(ar) :: t ->
                match nextIdle() with
                | Some(n) -> gs |> SetGSCitf n (fun c -> {c with tasks=[Task.CollectAR(ar);Task.Deliver({quantity=ar.quantity;resource=ar.rType;building=bldId})]})
                                |> SetGSBld bldId (WH({i with priorityTasks=t}))
                                |> BuildingTicFunc fs dt bldId
                | None -> gs
            | [] -> failwith "WH must ahve priority tasks at this point"
        | Blder i ->
            match i.priorityTasks with
            | PriorityTask.CollectAR(ar) :: t ->
                match nextIdle() with
                | Some(n) -> gs |> SetGSCitf n (fun c -> {c with tasks=[Task.CollectAR(ar);Task.Deliver({quantity=ar.quantity;resource=ar.rType;building=ar.destID})]})
                                |> SetGSBld bldId (Blder({i with priorityTasks=t}))
                                |> BuildingTicFunc fs dt bldId
                | None -> gs
            | [] -> failwith "Blder must have priority tasks at this point"
        | NotBuilt _ -> failwith "Not yet built building cannot have tasks"
        | Core(_) -> failwith "Core building cannot have tasks"
    else if canProduce then
        match nextIdle() with
        | Some(n) -> gs |> fs.fProduce n |> BuildingTicFunc fs dt bldId
        | None -> gs
    else match canCollect() with
            | Some(r,n,fromB,toB) ->
                match nextIdle() with
                | Some(eID) -> 
                    gs  |> fs.fCollectResources gs.citizens.[eID] r n gs.buildings.[fromB] gs.buildings.[toB]
                        |> BuildingTicFunc fs dt bldId
                | None -> gs
            | None -> 
                match b with
                | Core _  | WH _ | IO _ -> gs
                | Blder i ->
                    match i.blding with
                    | Some bID -> 
                        match gs |> GetGSBld bID with
                        | NotBuilt (nbi, nbb) ->
                            if (nbi.res |> Map.exists(fun k inv -> inv.acq < inv.req)) //if construction not yet fully resourced
                                then 
                                    if (i.e.emps |> List.exists(fun cid -> (GetGSCit cid gs).tasks |> List.exists(fun t -> t=Idle) |> not )) 
                                        then gs 
                                        elif i.e.emps.Length = 0 then gs
                                        else 
                                            let nextB = GetGSCanBuild gs
                                            (gs |> SetGSBld bldId (Blder({i with blding = nextB}))) |>  BuildingTicFunc fs dt bldId                                                  
                                else match nextIdle() with
                                        | Some(eID) ->
                                            gs  |> SetGSCit eID {(GetGSCit eID gs) with tasks=[Build(bID)]}
                                                |> BuildingTicFunc fs dt bldId
                                        | None -> gs 
                        | _ -> 
                            let fCancel gs id =  fs.fCitCancelTask id gs
                            let tempGS = i.e.emps |> List.fold(fCancel) gs
                            //Set all employees to Idle
                            let nextB = GetGSCanBuild gs
                            (tempGS |> SetGSBld bldId (Blder({i with blding = nextB}))) |>  BuildingTicFunc fs dt bldId
                    | None -> 
                        match GetGSCanBuild gs with
                        | Some(nextB) -> (gs |> SetGSBld bldId (Blder({i with blding = Some(nextB)}))) |>  BuildingTicFunc fs dt bldId
                        | None -> gs
                | NotBuilt (i,bld) -> if (ConComplete i) //its compeltely built
                                        then
                                            let AddConEvent id gs = gs |> AddEvent (BuildingConstructed(GetGSBld id gs))
                                            let newBld = if (bld.isEmp) 
                                                            then bld |> BldToEmp |> EmpSetEmpCap i.baseBldEmpCap |> EmpToBld
                                                            else bld
                                            gs |> SetGSBld bld.id bld |> AddEmpsToBld bld.id |> AddConEvent bld.id
                                        else gs 

type private testBuildingTicFunc() =
    static member Test() =
        ResetTests()
        SetTestName("BuildingTicFunc")
        
        let c0 = {Citizen<Point>.Default({x=0.0;y=0.0}) with id=0}
        let c1 = {c0 with id=1; tasks=[Task.Idle]}
        let c2 = {c1 with id=2}
        //Worker that should not be assigned
        let c3 = {c1 with id=3; tasks=[Task.MoveTo({x=0.0;y=0.0})]}

        let b0 = (Building<Loc2DBlock,Point>.Default({top=10;bottom=10;left=10;right=10},{x=1.0;y=1.0})).sID(0)
        let i0 = match b0 with | IO(x) -> x | _-> failwith "test data error"
        //iType None but none stored, 1 available to make 
        let i1 = {i0 with b={i0.b with id=1}; iType=None; iStored=0; oMax=10; iMax=10; oStored=5; inProgress=2; e={i0.e with emps=[1]}}
        let b1 = IO(i1)
        //iType not None but have resource to hand
        let i2 = {i1 with b={i1.b with id=2}; iType=Some(Resource.Food); iStored=1}
        let b2 = IO(i2)
        //several workes available to work
        let i3 = {i2 with b={i2.b with id=3}; e={i2.e with emps=[1;2]}; iStored=2; oMax=15}
        let b3 = IO(i3)
        let i4 = {i1 with b={i1.b with id=4}; e={i1.e with emps=[3]}}
        let b4 = IO(i4)
        let i5 = {i1 with b={i1.b with id=5}; iType=Some(Resource.Food)}
        let b5 = IO(i5)
        let i6 = {i2 with b={i2.b with id=6}; iStored=0}
        let b6 = IO(i6)
        //vaied list of employees
        
        let i7 = {i1 with b={i1.b with id=7}; e={i1.e with emps=[3;2;3]}}
        let b7 = IO(i7)
        let gs = {GameState<Loc2DBlock,Point>.Empty() with citizens=PV.ofSeq [c0; c1; c2; c3]; buildings=PV.ofSeq[b0;b1;b2;b3;b4;b5;b6;b7]; resourcesAvailable=Map([(Resource.Food, Map([(0,1)]))])}

        let mockCol c r id b1 b2 gs = {gs with events = NewCitizen(c) :: gs.events; citizens = gs.citizens |> PV.update c.id {c with tasks=[MoveTo({x=0.0;y=0.0})]}}
        let mockColT c gs = mockCol c Resource.Food 0 b0 b0 gs
        let mockProd c gs = 
            let cit = gs.citizens.[c]
            {gs with events= AssignedWork(cit) :: gs.events;  citizens = gs.citizens |> PV.update cit.id {cit with tasks=[MoveTo({x=0.0;y=0.0})]}} 
        let mockSomeCollectPoint r i gs = Some(0)
        let mockNoCollectPoint r i gs = None
        let mockFuncs = {fCollectResources=mockCol; fProduce=mockProd; fFindCollectPoint=mockSomeCollectPoint; fWHFindCollectPoint=WHFindCollectResource;fCitCancelTask=CancelTaskFunc}
        let f: (float -> int -> GameState<Loc2DBlock,Point> -> GameState<Loc2DBlock,Point>) = BuildingTicFunc mockFuncs  

        try
            ignore((f 0.5 101 gs))
            Test "If building ID doesnt exist then throw an exception" false
        with
        | _ ->  Test "If building ID doesnt exist then throw an exception" true


        Test "If can produce (iType None but none stored) and capacity worker exists that are idle or with empty tasks then set to produce" 
            ((f 0.5 1 gs) = mockProd c1.id gs )


        Test "If can produce (iStored > zero and itype not none) and worker exists that are idle or with empty tasks then set to produce" 
            ((f 0.5 2 gs) = mockProd c1.id gs)


        Test "Several workers will be assigned if possible" 
            ((f 0.5 3 gs) = (mockProd c1.id gs |> mockProd c2.id))

        Test "Workers not assigned if they are not currently empty or idle" 
            ((f 0.5 4 gs) = gs)

        Test "If produce can't be created due to input requriements, but goods can be sought, they will collect these, as many as required will collect these" 
            ((f 0.5 5 gs) = mockColT c1 gs) 

        Test "If goods will not be collected if they are not avilable" ((BuildingTicFunc {mockFuncs with fFindCollectPoint=mockNoCollectPoint} 0.5 6 gs) = gs)

        Test "If produce can't be created due to output space requirements, but goods can be sought, they will collect these, as many as required will collect these" 
            ((f 0.5 6 gs) = mockColT c1 gs) 

        Test "Ignores employees that cant work but continues to seek for ones that can" 
            ((f 0.5 7 gs) = mockProd c2.id gs)

        let ar1 = {quantity=5;loc={x=1.0;y=1.5};rType=Resource.RawFood;destID=1}
        let ar2 = {quantity=6;loc={x=1.5;y=2.5};rType=Resource.Food;destID=1}
        let gs_p = (gs |> SetGSBld 1 (IO({i1 with iType=Some(Resource.RawFood); priorityTasks=[PriorityTask.CollectAR(ar1);PriorityTask.CollectAR(ar2)]}))
                        |> SetGSCit 1 {c1 with empStatus=Working(1)})
        let gs_hp = BuildingTicFunc defaultBuildingTicFunctions 0.5 1 gs_p
        let gs_hp_after = {gs_hp with citizens= (gs_hp.citizens |> PV.update 1 {gs_p.citizens.[1] with tasks=[Task.CollectAR(ar1);Task.Deliver({building=1;resource=ar1.rType;quantity=ar1.quantity})]}); 
                                        buildings= (gs_hp.buildings |> PV.update 1 (IO({i1 with iType=Some(Resource.RawFood); priorityTasks=[PriorityTask.CollectAR(ar2)]})))}
                           
                           
        Test "High priority task done if set" (gs_hp=gs_hp_after)

        let c0 = {Citizen.Default(0) with id=0; empStatus=Working(2); tasks=[Idle]}
        let c1 = {c0 with id=1; empStatus=Working(3)}
        //can collect
        let whDef= WHinfo<int,int>.Default1D
        let whi2 = {whDef with b={BldBase.Default(0,0) with id=2}; stored=5;capacity=10;e={empDesc="";empMax=5;empCap=5;emps=[0]}}
                    |> WHSetInv (Resource.RawFood) (WHInv.Zero.sC 5)
        let wh2 = WH(whi2)
        //should collect food as less than target 
        let whi3 = {whi2 with b={whi2.b with id=3}; e={whi2.e with emps=[1]}; savedForTarget=3}
                    |> WHSetInv (Resource.Food) (WHInv.Zero.sT 3)
        let whi3_func = {whi2 with b={whi2.b with id=3}; e={whi2.e with emps=[1]}; savedForTarget=0} |> WHSetT Resource.Food 3 

        TestEq "Setting target using function equivalent to manual"
            whi3
            whi3_func

        let wh3 = WH(whi3)

        //Available to collect from
        let avail_i0 = {IOinfo<int,int>.Default1D with b = {IOinfo<int,int>.Default1D.b with id=0}; oStored=10; oType=Some(Resource.RawFood); delSize=5; shopFront=5}
        let avail_i1 = {avail_i0 with b={avail_i0.b with id=1}; shopFront=5; oType=Some(Resource.Food)}
        let avail0 = IO(avail_i0)
        let avail1 = IO(avail_i1)

        let gswh = {GameState<int,int>.Empty() with citizens = PV.ofSeq [c0;c1]; buildings=PV.ofSeq [avail0;avail1;wh2;wh3]; 
                                                    resourcesAvailable = Map([(Resource.Food,Map([(1,10)])); (Resource.RawFood,Map([(0,10)]))])}

        let gs_priority = gswh |> SetGSBld 2 (WH({whi2 with priorityTasks=[PriorityTask.CollectAR({quantity=3;loc=5;rType=Resource.Food;destID=2})]})) 
        let gs_priority_sol =  {gs_priority with    citizens= gs_priority.citizens |> PV.update 0 {c0 with tasks=[Task.CollectAR({quantity=3;loc=5;rType=Resource.Food;destID=2}); Task.Deliver({quantity=3;resource=Resource.Food;building=2})]} 
                                                    buildings = gs_priority.buildings |> PV.update 2 (WH(whi2))}
        
        TestEq "Priority task for a Warehouse" 
            (BuildingTicFunc defaultBuildingTicFunctions 0.5 2 gs_priority)
            gs_priority_sol

        TestEq "WH collect task runs" 
                (BuildingTicFunc defaultBuildingTicFunctions 0.5 2 gswh) 
                ( 
                (gswh   |> SetGSCit 0 {gswh.citizens.[0] with tasks=[MoveTo(avail0.location); Collect({quantity=5;resource=Resource.RawFood;building=0}); MoveTo(wh2.location); Deliver({quantity=5;resource=Resource.RawFood;building=2}) ]}
                        |> SetGSBld 2 (WH( {whi2 with enRoute=5; inv = whi2.inv |> PV.update (int Resource.RawFood) (whi2.inv.[(int Resource.RawFood)].sE 5)})) 
                        |> SetGSRAs (Map([(Resource.RawFood,Map([(0,5)]))]))
                        |> SetGSRAv (Map([(Resource.Food,Map([(1,10)])); (Resource.RawFood,Map([(0,5)]))]))
                        ))


        TestEq "WH fills up something less than target first"   
            (BuildingTicFunc defaultBuildingTicFunctions 0.5 3 gswh)
            ((gswh   |> SetGSCit 1 {gswh.citizens.[1] with tasks=[MoveTo(avail1.location);Collect({quantity=5;resource=Resource.Food;building=1});MoveTo(wh3.location);Deliver({quantity=5;resource=Resource.Food;building=3})]}
                    |> SetGSBld 3 (WH({whi3 with enRoute=5; savedForTarget=0; inv = whi3.inv |> PV.update (int Resource.Food) (whi3.inv.[(int Resource.Food)].sE 5)})) 
                    |> SetGSRAs (Map([(Resource.Food,Map([(1,5)]))]))
                    |> SetGSRAv (Map([(Resource.Food,Map([(1,5)])); (Resource.RawFood,Map([(0,10)]))]))
                    ))

        let gswh2 = gswh    |> SetGSBld 1 (IO({avail_i1 with oStored=0})) 
                            |> SetGSBld 0 (IO({avail_i0 with delSize=1}))
                            |> GSRemAv 1 Resource.Food 10  

        TestEq "WH will fill up if room and out of target, if target resource not available to collect"
            (BuildingTicFunc defaultBuildingTicFunctions 0.5 3 gswh2)
            (gswh2  |> SetGSCit 1 {gswh2.citizens.[1] with tasks=[MoveTo(avail0.location);Collect({quantity=1;resource=Resource.RawFood;building=0});MoveTo(wh3.location);Deliver({quantity=1;resource=Resource.RawFood;building=3})]}
                    |> SetGSBld 3 (WH({whi3 with enRoute=1; savedForTarget=3; inv = whi3.inv |> PV.update (int Resource.RawFood) (whi3.inv.[(int Resource.RawFood)].sE 1)})) 
                    |> SetGSRAs (Map([(Resource.RawFood,Map([(0,1)]))]))
                    |> SetGSRAv (Map([(Resource.Food,Map([(1,0)])); (Resource.RawFood,Map([(0,9)]))]))
                    )

        let gs_sw = {gswh with buildings = gswh.buildings |> PV.update 0 (IO({avail_i0 with shopFront=6}))|> PV.update 1 (IO({avail_i0 with shopFront=6}))}

        Test "WH wont collect if n < shop window" (gs_sw = BuildingTicFunc defaultBuildingTicFunctions 0.5 2 gs_sw)

        let wh_overtotalMax = WH({whi2 with capacity=4})
        let gs_resMaxTotal = gswh |> SetGSBld 2 wh_overtotalMax
        Test "WH wont collect if it will put it over total maxiumum" (gs_resMaxTotal = BuildingTicFunc defaultBuildingTicFunctions 0.5 2 gs_resMaxTotal)

        let gsBuild = gs |> SetGSBld 0 ((GetGSBld 0 gs).sConReqs Map.empty 0.0)
        let gsBuild_sol = gs |> AddEvent (BuildingConstructed(b0))

        TestEq "If not built building has zero time left then convert to actual building with empCap set to the value stored in the building"
            (BuildingTicFunc defaultBuildingTicFunctions 0.5 0 gsBuild)
            gsBuild_sol

        let blderInfo = BlderInfo.Default({top=10;bottom=10;left=10;right=10},{x=1.0;y=1.0})
        let blder = Blder({blderInfo with blding=Some(1); e={i2.e with emps=[1]}}).sID(3)
        let beingCon = ((gs |> GetGSBld 2).sConReqs (Map([(Resource.Food,1)])) 0.1)
        let gsBuild2 = gs |> SetGSBld 3 blder |> SetGSBld 2 ((gs |> GetGSBld 2).sConReqs (Map([(Resource.Food,1)])) 0.1) 

        let citBlt = {Citizen<Point>.Default({x=0.0;y=0.0}) with id=1;tasks=[Build(1)]}
        let gs_bldblt = ({gsBuild2 with beingBuilt=[2]} |> SetGSCit 1 citBlt)
        let gs_bldBlt_sol = gs_bldblt |> SetGSCit 1 {citBlt with tasks=[Idle]} |> SetGSBld 3 (Blder({blderInfo with blding=Some(2); e={i2.e with emps=[1]}}).sID 3) 
        TestEq "If builder is constructing a building that is built, find new building if possible"
            (BuildingTicFunc defaultBuildingTicFunctions 0.5 3 gs_bldblt)
            gs_bldBlt_sol

        let gsBuild3 = gsBuild2 |> SetGSBld 3 (Blder({blderInfo with blding=None}).sID(3))
        TestEq "If builder has no building, it will try to find one"
            ((BuildingTicFunc defaultBuildingTicFunctions 0.5 3 {gsBuild3 with beingBuilt=[2]}) |> GetGSBld 3)
            (Blder({blderInfo with blding=Some(2)}).sID(3))

        let c0 = {Citizen<Point>.Default({x=0.0;y=0.0}) with id=0}
        let c1 = {c0 with id=1; tasks=[Task.Idle]}
        let bld3 = (Blder({blderInfo with blding=Some(2); e ={i2.e with emps=[1]}; priorityTasks=[PriorityTask.CollectAR(ar1)] }).sID(3))
        let c1After = {c1 with tasks= [Task.CollectAR(ar1);Task.Deliver({building=1;resource=ar1.rType;quantity=ar1.quantity})]}
        let gsBuild4 = gsBuild3 |> SetGSBld 3 bld3
        let gsBuild4Sol = gsBuild4 
                            |> SetGSBld 3 ((Blder({blderInfo with blding=Some(2); e ={i2.e with emps=[1]}; priorityTasks=List.empty }).sID(3)))
                            |> SetGSCit 1 c1After
        TestEq "If builder has assigned building, it will first complete any priority tasks" 
            (BuildingTicFunc defaultBuildingTicFunctions 0.5 3 gsBuild4)
            gsBuild4Sol

        let bld4 = (Blder({blderInfo with blding=Some(2); e ={i2.e with emps=[1]}; priorityTasks=List.empty}).sID(3))
        let gsBuild5 =gsBuild4 |> SetGSBld 3 bld4
        let c1After2 = {c1 with tasks= [MoveTo(b0.location); Collect({quantity=1;resource=Resource.Food;building=0}); MoveTo(b1.location); Deliver({quantity=1;resource=Resource.Food;building=2})]}        
        let bld4After = match beingCon with | NotBuilt(i,x) -> NotBuilt ({i with res= i.res.Add(Resource.Food,{req=1;acq=0;enR=1})},x) | _ -> failwith "Not possible"
        let gsBuild5_sol = gsBuild5 |> SetGSCit 1 c1After2 |> GSRemAv 0 Resource.Food 1
                                    |> SetGSBld 2 bld4After
                                    |> GSAddAss 0 Resource.Food 1
        TestEq "If builder has assigned building, it will assign collect tasks if the building requires it (and no priority tasks)" 
            (BuildingTicFunc defaultBuildingTicFunctions 0.5 3 gsBuild5)
            gsBuild5_sol


        let bld5Before = match beingCon with | NotBuilt(i,x) -> NotBuilt ({i with res= i.res.Add(Resource.Food,{req=1;acq=1;enR=0})},x) | _ -> failwith "Not possible"
        let c1After3 = {c1 with tasks= [Build(2)]}
        let gsBuild6 = gsBuild5 |> SetGSBld 2 bld5Before 
        TestEq "For a builder: If all resources gathered then it will send employees to produce at the building" 
           (BuildingTicFunc defaultBuildingTicFunctions 0.5 3 gsBuild6)
           (gsBuild6 |> SetGSCit 1 c1After3)

 

        let bld6Before = match beingCon with | NotBuilt(i,x) -> NotBuilt ({i with res= i.res.Add(Resource.Food,{req=1;acq=0;enR=1})},x) | _ -> failwith "Not possible"
        let bld6Before2 = b2.sConReqs Map.empty 1.0 
        let gsBuild7 = gsBuild6 |> SetGSBld 1 bld6Before |> SetGSBld 2 bld6Before2
        let blder6After = (Blder({blderInfo with blding=Some(2); e ={i2.e with emps=[1]}; priorityTasks=List.empty}).sID(3))
        let c1After4 = {c1 with tasks= [Build(2)]}
        TestEq "For a builder: If all citizens are idle and cannot gather or produce, then will seek another building to build" 
            (BuildingTicFunc defaultBuildingTicFunctions 0.5 3 {gsBuild7 with beingBuilt=[1;2]})
            ( {gsBuild7 with beingBuilt=[1;2]} |> SetGSBld 3 blder6After |> SetGSCit 1 c1After4)

        Test "If building is built, then all employyess tasks are cancelled" false

        PrintTests() 



type UnitTests =
    static member run() =
        testBuildingTicFunc.Test()
