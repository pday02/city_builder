﻿namespace Taskns

open ResourceNs

type Order = {building:int;resource:Resource;quantity:int}
type Produce = {cit:int;building:int;resource:Resource;timeLeft:float}
type AbandonedResource<'P> = {quantity:int;loc:'P;rType:Resource;destID:int}

type Task<'P> = 
    | MoveTo of 'P 
    | Collect of Order
    | Deliver of Order
    | Produce of Produce
    | CollectAR of AbandonedResource<'P>
    | Build of int
    | Idle
    with
    member this.Desc(rest) : string =
        match this with
        | MoveTo(p) -> 
            match rest with
            | (x:Task<'P>) :: r -> x.Desc(r)
            | [] -> "Moving to " + p.ToString()
        | Collect(o) -> "Collecting " + o.resource.ToString() + " from " + o.building.ToString()
        | Deliver(o) -> "Delivering " + o.resource.ToString() + " to " + o.building.ToString()
        | Produce(p) -> "Producing " + p.resource.ToString()
        | Idle -> "Doing nothing"
        | CollectAR(r) -> "Collecting abandoned resource " + r.rType.ToString()
        | Build(f) -> "Building " + f.ToString()

    member this.Desc() : string = this.Desc(List.empty)
    override x.ToString() = 
        match x with
        | MoveTo(y) -> "MoveTo(" + y.ToString() + ")"
        | Collect(o) -> "Collect(" + o.ToString() + ")"
        | Deliver(o) -> "Deliver(" + o.ToString() + ")"
        | Produce(o) -> "Produce(" + o.ToString() + ")"
        | CollectAR(o) -> "CollectAR(" + o.ToString() + ")"
        | Idle -> "Idle"
        | Build(f) -> "Build("+f.ToString()+")"