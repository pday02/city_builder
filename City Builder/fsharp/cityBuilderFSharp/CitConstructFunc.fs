﻿module CitConstructFunc

open Global.Tester
open PointType
open GameState
open Loc2DBlockns
open Global.PV
open Buildingns
open GameState.funcs
open Citizenns
open Taskns

let CitConstructFunc dt citID bldID gs = 
    match GetGSBld bldID gs with
    | NotBuilt (i,b) -> 
        let c = GetGSCit citID gs
        if (c.location <> b.location) 
            then gs |> SetGSCit citID {c with tasks=Task.MoveTo(b.location)::c.tasks}
            else gs |> SetGSBld bldID (NotBuilt({i with time=max 0.0 (i.time - dt)},b))
    | _ -> failwith "Not possible to build a building that has already been built"

type private testCitConstructFunc() =
    static member Test() =
        ResetTests()
        SetTestName("CitConstructFunc")
        
        let b0 = (IO(IOinfo.Default(Loc2DBlock.Default,Point.Default))).sID 0
        let b1 = ((IO(IOinfo.Default(Loc2DBlock.Default,Point.Default))).sID 1).sConReqs Map.empty 10.0
        let b1_a = ((IO(IOinfo.Default(Loc2DBlock.Default,Point.Default))).sID 1).sConReqs Map.empty 8.5
        let b1_b = ((IO(IOinfo.Default(Loc2DBlock.Default,Point.Default))).sID 1).sConReqs Map.empty 0.0
        let c0 = {Citizen.Default(Point.Default) with id=0; location=b0.location; tasks=[(Task.Build 1)]}
        let c1 = {c0 with id=1; location = {Point.Default with x = Point.Default.x + 1.0}}
        let gs = {GameState<Loc2DBlock,Point>.Empty() with buildings=PV.ofSeq [|b0;b1|]; citizens=PV.ofSeq [|c0;c1|]}
                    

        TestEq "Build time reduced by the correct amount" 
            (CitConstructFunc 1.5 0 1 gs)
            (gs |> SetGSBld 1 b1_a)

        TestEq "Build time capped at zero if it would take it below zero"
            (CitConstructFunc 10.5 0 1 gs)
            (gs |> SetGSBld 1 b1_b)

        TestEq "If citizen isn't in the same location as the building they will move to the building"
            (CitConstructFunc 1.5 1 1 gs)
            (gs |> SetGSCit 1 {c1 with tasks=MoveTo(Point.Default)::c1.tasks})
        
        PrintTests()



type UnitTests =
    static member run() =
        testCitConstructFunc.Test()
