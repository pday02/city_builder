﻿namespace GameSettings

type GameSettings =
    {
        mapSize:int*int
        timeToProduce:float
    }
    with 
        static member Default() = 
            {
                mapSize = (100,100)
                timeToProduce = 60.0
            }
