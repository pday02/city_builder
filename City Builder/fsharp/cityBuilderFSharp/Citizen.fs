﻿namespace Citizenns

open Taskns
open ResourceNs
open Buildingns

type EmpStatus = Unemployed | Seeking | Working of int

type Citizen<'P> = {
        id:int;
        name:string;
        empStatus:EmpStatus;
        location:'P;
        tasks:Task<'P> list
        resCarried: Map<Resource,int>
    }
    with 
    static member Default(defLoc:'P) 
            =   {
                id = -1 
                location = defLoc
                name = "Default"
                empStatus = Unemployed
                tasks = List.empty
                resCarried = Map.empty
                }
    static member Default1D = Citizen<int>.Default(0)
    member this.setSeeking() = {this with empStatus=Seeking}
    member this.setWorking n = {this with empStatus=Working(n)}
    member this.setName n = {this with name = n}
    member this.workplace = match this.empStatus with | Working(w) -> w | _ -> failwith "Citizen not employed"

module funcs=
    let SetSeeking (c:Citizen<'P>) = c.setSeeking()
    let SetWorking id (c:Citizen<'P>) = c.setWorking(id)