﻿namespace PointType

type Point = {x:float; y:float}
    with 
    static member Default = {x=0.0;y=0.0}