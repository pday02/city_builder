﻿module CitProduceFunc

open Global.Tester
open Global.PV
open Global.General

open GameState
open Citizenns
open Taskns
open PointType
open Map2DBlockns
open ResourceNs
open Buildingns
open Loc2DBlockns

/// <summary>
/// Works a citizen producing a good.
/// </summary>
/// <param name="dt">The time the citizen has spent working on this this tic</param>
/// <param name="citId">The id of the citizen</param>
/// <param name="gs">The current gamestate</param>
let CitProduceFunc dt citId gs = 
    let c = gs.citizens |> PV.tryNth citId |> ThrowIfNone "Citizen ID not in citizens"
    match c.tasks with
    | Produce(p) :: _ -> 
        let b = gs.buildings |> PV.tryNth p.building |> ThrowIfNone "Building ID within task not found"
        match b with
        | IO(i) ->
            let resType = i.oType |> ThrowIfNone "Building doesnt have an output type"
            if (p.cit <> c.id) then failwith "Citizen ID and that with task do not match"
            if (p.timeLeft = 0.0) then failwith "No time left on produce task yet it hasnt been cleared"
            if (p.timeLeft < 0.0) then failwith "Time left is negative on produce task yet it hasnt been cleared"
            if (c.location = b.location) then
                if (i.oMax - i.oStored - i.inProgress < 0) then failwith "No room in building inventory to produce good"
                if (dt < p.timeLeft) then {gs with citizens=gs.citizens |> PV.update c.id {c with tasks=Produce({p with timeLeft=p.timeLeft - dt}) :: c.tasks.Tail}}
                else 
                    let newCit = {c with tasks=c.tasks.Tail}
                    let newB = IO({i with oStored=i.oStored + 1; inProgress=i.inProgress - 1})
                    let newEv = TaskComplete(newCit,c.tasks.Head) :: gs.events
                    let newAv = match gs.resourcesAvailable.TryFind(resType) with
                                | None -> gs.resourcesAvailable.Add(resType,Map([(b.id,1)]))
                                | Some(inv) -> match inv.TryFind(b.id) with
                                                | None -> gs.resourcesAvailable.Add(resType,inv.Add(b.id,1))
                                                | Some(n) -> gs.resourcesAvailable.Add(resType,inv.Add(b.id,n + 1))
                    {gs with 
                        citizens=gs.citizens |> PV.update c.id newCit;
                        buildings=gs.buildings |> PV.update b.id newB;
                        events=newEv;
                        resourcesAvailable=newAv}
            else {gs with citizens= gs.citizens |> PV.update c.id {c with tasks= MoveTo(b.location) :: c.tasks}}
        | WH(_) | Core(_) | Blder _ | NotBuilt _ -> failwith "Warehouses, builders, not built and core buildings do not produce goods" 
    | _ -> failwith "Citizen task not a produce type"

type private testCitProduceFunc() =
    static member Test() =
        ResetTests()
        SetTestName("CitProduceFunc")

        let b0 = Building.Default({top=0;right=0;left=0;bottom=0},{x=10.0;y=10.0})
        let i0 = match b0 with | IO(x) -> x | _ -> failwith "Error in test data"
        let i1 = {i0 with b={i0.b with id=1};oStored=5; oMax=10; inProgress=4;oType=Some(Resource.Food)}
        let b1 = IO(i1)
        //Building with no room
        let i2 = {i1 with b={i1.b with id=2};oMax=7}
        let b2 = IO(i2)

        //Not produce
        let c0 = {Citizen<Point>.Default({x=0.1;y=0.1}) with id=0; tasks=[Task.MoveTo({x=0.0;y=0.0})]}
        //works
        let c1 = {c0 with id=1; location=b1.location; tasks=[Task.Produce({cit=1;building=1;resource=Resource.Food;timeLeft=0.5})]}
        //Building id not found
        let c2 = {c1 with id=2; tasks=[Task.Produce({cit=2;building=1000;resource=Resource.Food;timeLeft=0.5})]}
        //time remaining zero
        let c3 = {c1 with id=3; tasks=[Task.Produce({cit=3;building=1;resource=Resource.Food;timeLeft=0.0})]}
        //time remaining negative
        let c4 = {c1 with id=4; tasks=[Task.Produce({cit=4;building=1;resource=Resource.Food;timeLeft= -1.0})]}
        //Cit id differs to task
        let c5 = {c1 with id=5; tasks=[Task.Produce({cit=1;building=1;resource=Resource.Food;timeLeft=0.5})]}
        //Cit in wrong location
        let c6 = {c1 with id=6; location={x=3.0;y=3.0};tasks=[Task.Produce({cit=6;building=1;resource=Resource.Food;timeLeft= 0.5})]}
        //Using building with no room
        let c7 = {c1 with id=7;tasks=[Task.Produce({cit=7;building=2;resource=Resource.Food;timeLeft=0.5})]}

        let gs = {GameState<Loc2DBlock,Point>.Empty() with
                    citizens=PV.ofSeq [c0;c1;c2;c3;c4;c5;c6];
                    buildings= PV.ofSeq [b0;b1;b2];
                    resourcesAvailable=Map([(Resource.Food, Map([(0,1);(1,2)]));(Resource.RawFood, Map([(0,4);(1,7)])) ])}
        
        try
            ignore((CitProduceFunc 0.5 1000 gs))
            Test "If citizen ID not found an exception is raised" false
        with
        | _ -> Test "If citizen ID not found an exception is raised" true 

        try
            ignore(CitProduceFunc 0.5 0 gs)
            Test "If citizen task is not producing an exception is raised" false
        with
        | _ -> Test "If citizen task is not producing an exception is raised" true

        try
            ignore(CitProduceFunc 0.5 2 gs)
            Test "If building ID not found an exception is raised" false
        with
        | _ -> Test "If building ID not found an exception is raised" true

        try
            ignore(CitProduceFunc 0.5 3 gs)
            Test "If time reamining is zero an exception is raised" false
        with
        | _ -> Test "If time reamining is zero an exception is raised" true

        try
            ignore(CitProduceFunc 0.5 4 gs)
            Test "If time remaining is negative an exception is raised" false
        with
        | _ -> Test "If time remaining is negative an exception is raised" true

        try
            ignore(CitProduceFunc 0.5 5 gs)
            Test "If citizen id is not equal to the produce task citizen id, an exception is raised" false
        with
        | _ -> Test "If citizen id is not equal to the produce task citizen id, an exception is raised" true


        Test "If the citizen is not in the right location then a move task will be added to the front"
            ((CitProduceFunc 0.5 6 gs).citizens.[6].tasks = MoveTo(b1.location) :: c6.tasks)

        try
            ignore(CitProduceFunc 1.5 7 gs)
            Test "When time is up, an exception is raised if the building does not have room" false
        with
        | _ -> Test "When time is up, an exception is raised if the building does not have room" true

        let gs2 = CitProduceFunc 1.5 1 gs
        Test "When time is up, the task is removed" (gs2.citizens.[1].tasks = [])

        Test "When time is up, 1 is added to the buildings inventory" 
            ((match gs2.buildings.[1] with IO(x) -> x.oStored | _ -> failwith "error in data") 
                = (match gs.buildings.[1] with IO(y) -> y.oStored + 1 | _ -> failwith "error in data"))

        Test "When time is up, 1 is removed from in progress" 
            ((match gs2.buildings.[1] with IO(x) -> x.inProgress |_-> failwith "data error" )= 
            (match gs.buildings.[1] with IO(y) -> y.inProgress - 1 | _ -> failwith "error in data"))

        Test "When time is up, 1 is made available to the gamestate"
            (gs2.resourcesAvailable.[Resource.Food].[1] = gs.resourcesAvailable.[Resource.Food].[1] + 1)

        let gs3 = CitProduceFunc 0.3 1 gs
        Test "If time is not up, time decreases by the provided amount" 
            (gs3.citizens.[1].tasks = [Task.Produce({cit=1;building=1;resource=Resource.Food;timeLeft=0.2})])

        Test "If time is not up, then the only thing that changes is the task timer"
            ({gs3 with citizens=gs3.citizens |> PV.update 1 c1} = gs)

        Test "Time being up exactly or over produces the same result"
            ((CitProduceFunc 0.5 1 gs) = gs2)

        Test "When time is up, an event is added to the list"
            (gs2.events.Head = TaskComplete(gs2.citizens.[1], c1.tasks.Head)) 

        PrintTests()



type UnitTests =
    static member run() =
        testCitProduceFunc.Test()
