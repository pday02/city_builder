﻿module SwapEmployeesFunc

open Global.Tester
open Global.PV
open CancelTaskFunc
open Citizenns
open Buildingns
open GameState
open GameState.funcs
open Citizenns.funcs
open Buildingns.funcs
open Taskns

let SwapEmployeesFunc cancelTask bid1 i1 bid2 i2 gs = 
    let b1 = GetGSBld bid1 gs |> BldToEmp
    let b2 = GetGSBld bid2 gs |> BldToEmp
    if (i1 < 0 || i2 < 0) then failwith "Index negative or greater than cap"
    if (i1 >= b1.empCap || i2 >= b2.empCap || (i1 >= b1.emps.Length && i2 >= b2.emps.Length)) then gs
    else
        let cid1 = if (b1.emps.Length <= i1) then None else Some(b1.emps.[i1])
        let cid2 = if (b2.emps.Length <= i2) then None else Some(b2.emps.[i2])
        let CancelTask co gs = match co with None -> gs | Some(n) -> cancelTask n gs
        let UpdateCitf co newB gs = match co with None -> gs | Some(n) -> SetGSCitf n (SetWorking newB) gs 
        let UpdateBldf co coOther newB gs = 
            let bTemp = GetGSBld newB gs |> BldToEmp
            match co, coOther with
                | None, None -> failwith "Should be impossible" 
                | None, Some(n) -> 
                    SetGSBld newB (bTemp.sEmps (bTemp.emps |> List.filter((<>) n)) |> EmpToBld) gs
                | Some(n), None ->
                    SetGSBld newB (bTemp.sEmps (bTemp.emps @ [n]) |> EmpToBld) gs 
                | Some(n1), Some(n2) ->
                    SetGSBld newB (bTemp.sEmps (bTemp.emps |> List.map(fun x -> if (x=n2) then n1 else x)) |> EmpToBld) gs                                               
        gs  |> CancelTask cid1 |> CancelTask cid2 |> UpdateCitf cid1 bid2 |> UpdateCitf cid2 bid1
            |> UpdateBldf cid1 cid2 bid2 |> UpdateBldf cid2 cid1 bid1
    

type private testSwapEmployeesFunc() =
    static member Test() =
        ResetTests()
        SetTestName("SwapEmployeesFunc")

        //Two switch round properly

        let c0 : Citizen<int> = {Citizen<int>.Default(1) with id=0; empStatus=Working(1); tasks=[Task.MoveTo(10)]}
        let c1 = {c0 with id=1}
        let c2 = {c0 with id=2}
        let c3 = {c0 with id=3; empStatus=Working(2)}
        let c4 = {c3 with id=4}
        let c5 = {c3 with id=5}

        let b0 = IO({IOinfo.Default(0,0) with e= {BldEmpl.Default with empMax=10; empCap=8}}).sID(0)
        let i0 = match b0 with | IO(x) -> x | _ -> failwith "error in test"
        let i1 = {i0 with e = {i0.e with emps=[2;1;0]}; b = {i0.b with id=1}}
        let b1 = IO(i1)
        let i2 = {i0 with e = {i0.e with emps=[5;4;3]}; b = {i0.b with id=2}}
        let b2 = IO(i2)

        let gs = {GameState<int,int>.Empty() with citizens=PV.ofSeq [c0;c1;c2;c3;c4;c5]; buildings=PV.ofSeq [b0;b1;b2]}
        let TestSwapFunc = SwapEmployeesFunc CancelTaskFunc
        let gs2 = TestSwapFunc 1 1 2 1 gs

        Test "Two switch round properly"
            (
                gs2.buildings.[1] = IO({i1 with e={i1.e with emps=[2;4;0]}}) &&
                gs2.buildings.[2] = IO({i2 with e={i2.e with emps=[5;1;3]}}) &&
                gs2.citizens.[1] = {c1 with tasks=[Task.Idle]; empStatus=Working(2)} &&
                gs2.citizens.[4] = {c4 with tasks=[Task.Idle]; empStatus=Working(1)}
            )

        let gs3 = TestSwapFunc 1 1 2 6 gs

        Test "One switches round properly"
            (
                gs3.buildings.[1] = IO({i1 with e={i1.e with emps=[2;0]}}) &&
                gs3.buildings.[2] = IO({i2 with e={i2.e with emps=[5;4;3;1]}}) &&
                gs3.citizens.[1] = {c1 with tasks=[Task.Idle]; empStatus=Working(2)}
            )
        

        //Faulty if index is higher than current capacity or less than zero

        try
            ignore(TestSwapFunc 1 -1 2 9 gs)
            Test "Exception raised if index negative" false
        with
        | _ -> Test "Exception raised if index negative" true

        Test "Original gamestate returned if either value above cap" ((TestSwapFunc 1 1 2 9 gs) = gs)



        PrintTests()



type UnitTests =
    static member run() =
        testSwapEmployeesFunc.Test()
