﻿namespace Buildingns

open ResourceNs
open Loc2DBlockns
open PointType
open PriorityTask
open Global.PV
open ResourceNs.funcs


///A building that can be held in the world. 
///LocType s the generic type used to denote its location in the world

type BldBase<'L,'P> = {id:int; name:string; area:'L; location:'P; label:string}
    with 
        static member Default(defArea:'L,defLoc:'P) = {id= -1;name="";area=defArea;location=defLoc;label="Default"}
        static member Default2D = BldBase<Loc2DBlock,Point>.Default(Loc2DBlock.Default,Point.Default) 
        member this.sID n = {this with id = n}
        member this.sName s = {this with name = s}
        member this.sArea a = {this with area=a}
        member this.sLocation p = {this with location=p}
        member this.sLabel l = {this with label=l} 

type BldEmpl = {empDesc:string; empMax:int; empCap:int; emps:int list}
    with 
        static member Default = {empDesc="";empMax=0; empCap=0;emps=List.empty}
        member this.sEmpDesc s = {this with empDesc=s}
        member this.sEmpMax n = {this with empMax=n}
        member this.sEmpCap n = {this with empCap=n}
        member this.sEmps l = {this with emps=l}

type IOinfo<'L,'P> = {  b:BldBase<'L,'P>; 
                        e:BldEmpl
                        iType:Resource option; 
                        oType:Resource option; 
                        iMax:int;
                        oMax:int;
                        inProgress:int;
                        enRoute:int;
                        iStored:int;
                        oStored:int
                        timeToProduce:float
                        shopFront:int //The amount of output goods that will be displayed on the front of the shop, the rest will be placed in storage. Only warehouses are affected by this
                        delSize:int //The size of a delivery that can be carried to the warehouse 
                        priorityTasks: PriorityTask<'P> list
                        }
    with
        member this.isSomeOType = this.oType.IsSome
        member this.isSomeIType = this.iType.IsSome
        static member Default(defArea:'L,defLoc:'P) = 
            {   b=BldBase.Default(defArea,defLoc)
                e = BldEmpl.Default
                iType=None
                oType=None
                iMax=0
                oMax=0
                inProgress=0
                enRoute=0
                iStored=0
                oStored=0
                timeToProduce=0.0 
                shopFront=0
                delSize=1
                priorityTasks = List.empty
            }
        static member Default2D = IOinfo.Default(Loc2DBlock.Default,Point.Default)
        static member Default1D = IOinfo.Default(0,0)
        member x.sIType r = {x with iType=Some(r)}
        member x.sOType r = {x with oType=Some(r)}
        member x.sIMax n = {x with iMax=n}
        member x.sOMax n = {x with oMax=n}
        member x.sTimeToProduce n = {x with timeToProduce=n}
        member x.sShopFront n = {x with shopFront=n}
        member x.sDelSize n = {x with delSize=n}

type WHInv = {name:string;res:Resource;current:int; target:int; enRoute:int}
    with 
    static member Zero = {name="default";res=enum -1; current=0; target=0; enRoute=0}
    member this.sC n = {this with current=n}
    member this.sT n = {this with target=n}
    member this.sE n = {this with enRoute=n} 
    member this.sCf f = {this with current=f (this.current)}
    member this.sTf f = {this with target= f this.target}
    member this.sEf f = {this with enRoute=f this.enRoute} 
    static member sC (i:WHInv) = i.sC
    static member sT (i:WHInv) = i.sT
    static member sE (i:WHInv) = i.sE
    static member sCf (i:WHInv) = i.sCf
    static member sTf (i:WHInv) = i.sTf
    static member sEf (i:WHInv) = i.sEf
    
   

type WHinfo<'L,'P> = {
                        b:BldBase<'L,'P>
                        e:BldEmpl
                        inv: WHInv PV
                        enRoute:int
                        stored:int
                        savedForTarget:int //Unused space that has been saved to meet a target
                        capacity:int
                        priorityTasks:PriorityTask<'P> list
                        }
    with static member Default(defArea:'L, defLoc:'P) = 
            {
            b=BldBase.Default(defArea,defLoc); 
            e = BldEmpl.Default; 
            enRoute=0;
            capacity = 0;
            inv = Array.create (MaxResN + 1) WHInv.Zero 
                        |> Array.mapi (fun i x -> {x with name = ResName (enum<Resource> i); res = enum i})
                        |> PV.ofSeq;
            stored=0;
            savedForTarget=0;
            priorityTasks = List.empty
            }
         static member Default2D = WHinfo.Default(Loc2DBlock.Default,Point.Default)
         static member Default1D = WHinfo.Default(0,0)
         member x.sE e = {x with e=e}
         member x.sB b = {x with b = b}
         member x.sCapacity n ={x with capacity=n}

type BlderInfo<'L,'P> = {
    b: BldBase<'L,'P>
    e:BldEmpl
    priorityTasks:PriorityTask<'P> list
    blding : int option
}
with 
    static member Default(defArea:'L, defLoc:'P) = {
                                                        b=BldBase.Default(defArea,defLoc); 
                                                        e=BldEmpl.Default; 
                                                        priorityTasks=List.empty;
                                                        blding=None}

type Employer<'L,'P> =
    | IO of IOinfo<'L,'P>
    | WH of WHinfo<'L,'P>
    | Blder of BlderInfo<'L,'P>
    with
    member this.core = 
        match this with
        | IO(i) -> i.e
        | WH(i) -> i.e
        | Blder(i) -> i.e
    member this.sCore c =
        match this with
        | IO(i) -> IO({i with e=c})
        | WH(i) -> WH({i with e=c})
        | Blder i -> Blder {i with e=c}
    member this.empCap = this.core.empCap
    member this.emps = this.core.emps
    member this.empMax = this.core.empMax
    member this.empDesc = this.core.empDesc
    member this.sEmps l = this.sCore {this.core with emps=l}
    member this.sEmpCap n = this.sCore {this.core with empCap=n}
    member this.sEmpMax n = this.sCore {this.core with empMax=n}
    member this.sEmpDesc d = this.sCore {this.core with empDesc=d}

type MatInv = {req:int; acq:int;enR:int}
type ConInfo = {res:Map<Resource,MatInv>; time:float; baseBldEmpCap:int}

type Building<'L,'P> =
    | IO of IOinfo<'L,'P>
    | WH of WHinfo<'L,'P>
    | NotBuilt of (ConInfo*Building<'L,'P>)
    | Blder of BlderInfo<'L,'P>
    | Core of BldBase<'L,'P>
    with
    member this.core = 
        match this with
        | IO(i) -> i.b
        | WH(i) -> i.b
        | Blder i -> i.b
        | NotBuilt(i) -> (snd i).core
        | Core(i) -> i
    member this.sCore c =
        match this with
        | IO(i) -> IO({i with b=c})
        | WH(i) -> WH({i with b=c})
        | Blder i -> Blder {i with b=c}
        | NotBuilt(i) -> NotBuilt(fst i, (snd i).sCore c)
        | Core(i) -> Core(c)
    member this.iEmp =
        match this with
        | IO(i) -> Some(i.e)
        | WH(i) -> Some(i.e)
        | Blder i -> Some i.e
        | NotBuilt(i) -> None
        | Core(i) -> None
    member this.iWH =
        match this with
        | WH(i) -> Some(i)
        | _ -> None
    override x.ToString() =
        match x with
        | IO(i) -> "IO(" + i.ToString() + ")"
        | WH(i) -> "WH(" + i.ToString() + ")"
        | Blder i -> "Blder(" + i.ToString() + ")"
        | NotBuilt(i) -> "NotBuilt(" + (fst i).ToString() + ", " + (snd i).ToString() + ")"
        | Core(i) -> "Core(" + i.ToString() + ")"
    member this.isEmp = this.iEmp.IsSome
    member this.iIO = match this with | IO(x) -> Some(x) | _ -> None
    member x.id = x.core.id
    member x.sID n = n |> x.core.sID |> x.sCore
    member x.name = x.core.name
    member x.sName s = s |> x.core.sName |> x.sCore
    member x.area = x.core.area
    member x.sArea a = a |> x.core.sArea |> x.sCore
    member x.location = x.core.location
    member x.sLocation a = a |> x.core.sLocation |> x.sCore
    member x.label = x.core.label
    member x.sLabel a = a |> x.core.sLabel |> x.sCore
    /// <summary>
    /// Changes the building into one which requires constructing
    /// </summary>
    /// <param name="r">The PV of resource quantities required</param>
    /// <param name="t">The time the building will take to construct</param>
    member x.sConReqs (r:Map<Resource,int>) t =
        match x with 
        | NotBuilt _ -> failwith "Building is already a NotBuilt type"
        | IO(_) | WH(_) | Blder _ as b -> 
            let inv = r |> Map.map(fun k n -> {req=n;enR=0;acq=0})
            let newE = (b.iEmp.Value.sEmpCap 0) //Sets the unconstructed base building to have no employees
            let baseEmpCap = b.iEmp.Value.empCap
            let newB = match b with
                            | WH(i) -> WH({i with e = newE})
                            | IO(i) -> IO({i with e = newE})
                            | Blder i -> Blder {i with e= newE}
                            | Core _ | NotBuilt _ -> failwith "not possible value" 
            NotBuilt({res=inv;time=t;baseBldEmpCap=baseEmpCap},newB)
        | Core _ -> failwith "Core building type cannot be constructed"
    static member Default(defArea:'L, defLoc:'P) = IO(IOinfo.Default(defArea,defLoc))
    static member Default1D = IO(IOinfo.Default(0,0))
    static member DefaultIO(defArea:'L, defLoc:'P) = IO(IOinfo.Default(defArea,defLoc))
    

module funcs=
    open Global.General

    let SetEmps e b = {b with emps = e}

    let EmpToBld (e:Employer<'L,'P>) =
        match e with
        | Employer.IO(i) -> IO(i)
        | Employer.WH(i) -> WH(i)
        | Employer.Blder i -> Blder i

    let rec BldToEmpTry (b:Building<'L,'P>) =
        match b with
        | IO(i) -> Some(Employer.IO(i))
        | WH(i) -> Some(Employer.WH(i))
        | Blder i -> Some (Employer.Blder i)
        | NotBuilt(i) -> None
        | Core(_) -> None

    let BldToEmp b = b |> BldToEmpTry |> ThrowIfNone "Not an employer type" 

    let BldSetID n (b:Building<'L,'P>) = b.sID(n)

    let EmpSetCore c (e:Employer<'L,'P>) = e.sCore(c)

    let EmpSetEmpMax n (e:Employer<'L,'P>) = e.sCore({e.core with empMax=n})

    let EmpSetEmpCap n (e:Employer<'L,'P>) = e.sCore({e.core with empCap=n})

    let EmpSetEmps l (e:Employer<'L,'P>) = e.sCore({e.core with emps=l})

    let WHSetInv (r:Resource) (i:WHInv) (wh:WHinfo<'L,'P>) = 
        {wh with inv = wh.inv |> PV.tryUpdate (int r) {i with name=ResName r; res=r} |> ThrowIfNone ("Resource id " + (int r).ToString() + " not found")}

    let WHGetInv (r:Resource) (wh:WHinfo<'L,'P>) = 
        wh.inv |> PV.tryNth (int r) |> ThrowIfNone ("Resource id " + (int r).ToString() + " not found")

    let WHSetInvF (f:WHInv -> WHInv) (r:Resource) (wh:WHinfo<'L,'P>) =
        WHSetInv r (wh |> WHGetInv r |> f) wh  

    let WHStored r wh = (wh |> WHGetInv r).current
    let WHTarget r wh = (wh |> WHGetInv r).target
    let WHEnRoute r wh = (wh |> WHGetInv r).enRoute

    /// <summary>
    /// Provides how much of the target amount of this resource has not yet been found
    /// </summary>
    /// <param name="r"></param>
    /// <param name="wh"></param>
    let WHUnusedT r wh = max 0  ((WHTarget r wh) - (WHStored r wh) - (WHEnRoute r wh))

    let private WHApplyInvC f r n wh =
        let newInv = wh  |> WHSetInv r ((wh |> WHGetInv r |> WHInv.sCf ) ((f) n))      
        {newInv with 
            stored = newInv.stored - (WHStored r wh) + (WHStored r newInv)
            savedForTarget = newInv.savedForTarget - (WHUnusedT r wh) + (WHUnusedT r newInv)}
    
    let private WHApplyInvER f r n wh =
        let newInv = wh  |> WHSetInv r ((wh |> WHGetInv r |> WHInv.sEf ) ((f) n))
        {newInv with 
            enRoute = newInv.enRoute - (WHEnRoute r wh) + (WHEnRoute r newInv)
            savedForTarget = newInv.savedForTarget - (WHUnusedT r wh) + (WHUnusedT r newInv)}

    
    let WHAddInv r n wh = //Does not alter resource available in gamestate
        WHApplyInvC (+) r n wh
    
    let WHRemInv r n wh = //Does not alter resource available in gamestate
        WHApplyInvC (fun x y-> y - x) r n wh

    let WHAddER r n wh =
        WHApplyInvER (+) r n wh

    let WHRemER r n wh =
        WHApplyInvER (fun x y -> y - x) r n wh
         

    /// <summary>
    /// Moves resource from EnRoute to in the Inventory
    /// </summary>
    /// <param name="r"></param>
    /// <param name="n"></param>
    /// <param name="wh"></param>
    let WHErToInv r n wh = wh |> WHRemER r n |> WHAddInv r n


    /// <summary>
    /// Sets a target quantity for a resource
    /// </summary>
    /// <param name="r">The resource to alter</param>
    /// <param name="n">The quantity to set it to</param>
    /// <param name="wh">The arehouse info to alter</param>
    let WHSetT r n wh = 
        let newInv = (wh |> WHSetInvF (fun x -> {x with target=n}) r)
        {newInv with 
            savedForTarget= wh.savedForTarget - (WHUnusedT r wh) + (WHUnusedT r newInv)}

    /// <summary>
    /// the amount of free space in the warehouse
    /// </summary>
    /// <param name="wh"></param>
    let WHFS wh = wh.capacity - wh.enRoute - wh.stored - wh.savedForTarget


    /// <summary>
    /// The amount fo free sapce for a given resource in a warehouse
    /// </summary>
    /// <param name="r"></param>
    /// <param name="wh"></param>
    let WHFSRes r wh = (WHFS wh) + (WHUnusedT r wh)

    let WHInfoSid n wh : WHinfo<'L,'P> = {wh with b = {wh.b with id = n}}

    let ConComplete (c:ConInfo) =
        c.time <= 0.0 && (c.res |> Map.exists(fun k i -> i.acq <> i.req) |> not)
   
    let NBAddER r n (i:ConInfo) =
        {i with res = i.res.Add(r, {i.res.[r] with enR = (i.res.[r].enR + n) })}
        
    let NBRemER r n (i:ConInfo) =  
        {i with res = i.res.Add(r, {i.res.[r] with enR = (i.res.[r].enR - n) })}   

    let private NBAddAcq r n (i:ConInfo) = 
        {i with res = i.res.Add(r, {i.res.[r] with acq = (i.res.[r].acq + n) })}

    let NBErToAcq r n (i:ConInfo) =
        i |> NBRemER r n |> NBAddAcq r n