﻿namespace ResourceNs

type Resource = RawFood=0 | Food=1 | Stone=2 | Wood=3


//let allTags = Enum.GetValues(typeof<Resource>);;

module funcs = 
    open System
    open Global.Enum
    let MaxResN = EnumArrayAll<Resource>() |> Array.map(int) |> Array.max
 
    let ResName r = r.ToString()