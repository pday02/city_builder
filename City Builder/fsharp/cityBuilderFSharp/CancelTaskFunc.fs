﻿module CancelTaskFunc

open Global.Tester
open Global.General
open Global.Map
open Global.PV
open GameState
open Citizenns
open ResourceNs
open Buildingns
open Taskns
open GameState.funcs
open Buildingns.funcs

/// <summary>
/// Cancels the citizens task as provided by an id
/// </summary>
/// <param name="id">The id of the citizen to cancel the task of</param>
/// <param name="gs">The current gamestate</param>
let rec CancelTaskFunc id gs = 
    let c = gs.citizens |> PV.tryNth id |> ThrowIfNone ("Citizen id " + string(id) + " not found in citizen array")
    match c.tasks with
    
    | Task.Idle :: _ -> gs
    

    | [] -> {gs with citizens = gs.citizens |> PV.update id {c with tasks=[Idle]}}
    

    | Task.Produce(p) :: t -> 
        let b = gs.buildings |> PV.tryNth p.building |> ThrowIfNone ("Building id + " + string(p.building) + " not found in building array")
        match b with
        | IO(i) ->
            let nextgs = {gs with 
                            citizens = gs.citizens |> PV.update id {c with tasks = t}
                            buildings = gs.buildings |> PV.update b.id (IO({i with inProgress = i.inProgress - 1}))
                            }
            CancelTaskFunc id nextgs
        | _ -> failwith ("Unexpected type of building to produce " + b.GetType().ToString())
    
    
    | Task.MoveTo(l) :: t -> CancelTaskFunc id {gs with citizens = gs.citizens |> PV.update id {c with tasks = t}}
    

    | Task.Collect(o) :: t -> 
        let b = gs.buildings |> PV.tryNth o.building |> ThrowIfNone ("Building id + " + string(o.building) + " not found in building array")
        let nextgs = {gs with 
                        citizens = gs.citizens |> PV.update id {c with tasks = t}
                        resourcesAvailable = AddToMap o.resource b.id o.quantity gs.resourcesAvailable} 
        CancelTaskFunc id nextgs
    

    | Task.Deliver(o) :: t -> 
        let b = gs.buildings |> PV.tryNth o.building |> ThrowIfNone ("Building id + " + string(o.building) + " not found in building array")
        if (c.resCarried.ContainsKey(o.resource) && c.resCarried.[o.resource] >= o.quantity) then //If citizen was carrying the reource, abandon it and raise a priority
            let newResCar = c.resCarried.Add(o.resource, c.resCarried.[o.resource] - o.quantity)
            let ar = {quantity=o.quantity;loc=c.location;rType=o.resource; destID= o.building}
            let newEv = ResourceDropped(ar) :: gs.events 
            let tempgs = {gs with
                                    citizens=gs.citizens |> PV.update id {c with tasks = t; resCarried=newResCar}
                                    events = newEv
                                    }
            match b with
            | IO(i) ->
                    let nextgs = {tempgs with buildings=gs.buildings |> PV.update b.id (IO({i with priorityTasks = PriorityTask.CollectAR(ar) :: i.priorityTasks}))}
                    CancelTaskFunc id nextgs
              
            | WH(i) ->
                    let nextgs = {tempgs with buildings=gs.buildings |> PV.update b.id (WH({i with priorityTasks = PriorityTask.CollectAR(ar) :: i.priorityTasks}))}
                    CancelTaskFunc id nextgs
            
            | NotBuilt(i,nb) -> 
                let blderID = match c.empStatus with | Working(n) -> n | _ -> failwith "Citizen is delivering to an unbuilt building but is unemployed" 
                let newBlder = match GetGSBld blderID tempgs with | Blder i -> {i with priorityTasks= PriorityTask.CollectAR(ar) :: i.priorityTasks } | _ -> failwith "Should be a builder if delivering to an unbuilt building"
                tempgs |> SetGSBld blderID (Blder(newBlder)) |> CancelTaskFunc id  
            
            | Core(_) | Blder _ -> failwith "Deliveries are not possible to core building types"
        
        else //Abandon resource task adds on to enRoute so deliver can always remove (so that when havent collected the enroute amount can be ignored)
            let tempgs = {gs with citizens=gs.citizens |> PV.update id {c with tasks = t}}
            match b with
            
            | IO(i) ->
                    let nextgs = {tempgs with buildings=gs.buildings |> PV.update b.id (IO({i with enRoute=i.enRoute - o.quantity}))}
                    CancelTaskFunc id nextgs
            
            | WH(i) -> tempgs |> SetGSBld o.building (WH(i |> WHRemER o.resource o.quantity)) |> CancelTaskFunc id
            
            | NotBuilt (i,nb) -> tempgs |> SetGSBld b.id (NotBuilt(i |> NBRemER o.resource o.quantity,nb)) |> CancelTaskFunc id
            
            | Core(_) | Blder _ -> failwith "Deliveries are not possible to core building types"
    

    | Task.CollectAR(ar) :: t ->
        let b = gs.buildings |> PV.tryNth ar.destID |> ThrowIfNone ("Building id + " + string(ar.destID) + " not found in building array")
        let tempgs = gs |> SetGSCit id {c with tasks=t} // |> AddEvent (ResourceDropped(ar)) removed resource being dropped if it wasnt picked up (deposit will do this)
        match b with //Need to add to enroute (will be removed on deliver) and add priority task
        
        | IO(i) -> 
            let nextgs = tempgs |> SetGSBld b.id (IO({i with enRoute=i.enRoute + ar.quantity; priorityTasks=PriorityTask.CollectAR(ar) :: i.priorityTasks}))
            CancelTaskFunc id nextgs
        
        
        | WH(i) -> tempgs   |> SetGSBld b.id (WH({(i |> WHAddER ar.rType ar.quantity) with priorityTasks=PriorityTask.CollectAR(ar) :: i.priorityTasks}))
                            |> CancelTaskFunc id
        
        
        | NotBuilt (i,nb) -> 
            let blderID = match c.empStatus with | Working(n) -> n | _ -> failwith "Citizen is delivering to an unbuilt building but is unemployed" 
            let newBlder = match GetGSBld blderID tempgs with | Blder i -> {i with priorityTasks= PriorityTask.CollectAR(ar) :: i.priorityTasks } | _ -> failwith "Should be a builder if delivering to an unbuilt building"
            tempgs 
                |> SetGSBld b.id (NotBuilt(i |> NBAddER ar.rType ar.quantity,nb))
                |> SetGSBld blderID (Blder(newBlder))
                |> CancelTaskFunc id
        
        
        | Core(_) | Blder _ -> failwith "Deliveries not possible to core building types"
    
    
    | Task.Build _ :: t -> gs   |> SetGSCit id {c with tasks=t} 
                                |> CancelTaskFunc id

type private testCancelTaskFunc() =
    static member Test() =
        ResetTests()
        SetTestName("CancelTaskFunc")

        let c0 = {Citizen<int>.Default(0) with id=0; tasks = [Task.Idle]}
        let c1 = {Citizen<int>.Default(0) with id=1; tasks = []}
        let c2 = {Citizen<int>.Default(0) with id=2; tasks = [Task.Produce({cit=2;building=0;resource=Resource.Food;timeLeft=0.5})]}
        let b0 = IO({IOinfo.Default(1,1) with inProgress=5; enRoute=8}).sID(0)
        let i0 = match b0 with | IO(x) -> x | _ -> failwith "error writing test" 
        let c3 = {Citizen<int>.Default(0) with id=3; tasks = [Task.Collect({building = b0.id;resource=Resource.Food;quantity=5})]}
        let c4 = {c0 with id=4; tasks=[Task.Deliver({building = b0.id;resource=Resource.RawFood;quantity=5})];resCarried=Map([(Resource.RawFood, 3)])}
        let c5 = {c4 with id=5;  resCarried=Map([(Resource.RawFood, 11)]); location=11}
        let c6 = {c4 with id=6; tasks=c3.tasks @ Task.MoveTo(50) :: c4.tasks}
        let ar : AbandonedResource<int> = {quantity=5;loc=13;rType=Resource.RawFood;destID= 0}
        let c7 = {c4 with id=7; tasks=Task.CollectAR(ar) :: c4.tasks}

        //Citizens for warehouse test
        //Hasn't yet collected as it isnt in inventory
        let c8 = {c0 with id=8; tasks=[Task.Deliver({building = 1;resource=Resource.Food;quantity=3})];resCarried=Map([(Resource.Food, 2)])}
        //Has collected as resource is in inventory
        let c9 = {c0 with id=9; location=2; tasks=[Task.Deliver({building = 1;resource=Resource.Food;quantity=3})];resCarried=Map([(Resource.Food, 4)])}
        //Is still to collect an abandoned resource
        let arWH = {quantity=3;loc=13;rType=Resource.Food;destID= 1}
        let c10 = {c9 with id=10; tasks=Task.CollectAR(arWH) :: c9.tasks ; resCarried=Map([(Resource.Food, 1)])}

        let whdef = WHinfo<int,int>.Default1D
        let i1 = {whdef with capacity=15}
                    |> WHSetT Resource.Food 10
                    |> WHAddInv Resource.Food 5
                    |> WHAddER Resource.Food 4
                    |> WHInfoSid 1
        let b1 = WH(i1)

        //Create a building being built
        let b2 = match b1.sID(2).sConReqs (Map([(Resource.Food,3)])) 0.1 with   | NotBuilt(i,b) -> NotBuilt(i |> NBAddER Resource.Food 2,b)
                                                                                | _ -> failwith "Not possible"
        let b2i,b2b = match b2 with | NotBuilt(i,b) -> i,b | _ -> failwith "Not possible"
        //Create a builder
        let b3i = {BlderInfo.Default(0,0) with blding= Some(2)}
        let b3 = (Blder (b3i)).sID 3
        
        //Citizen delivering 

        let c11 =  {c9 with id=11; empStatus=Working(3); tasks=[Task.Deliver({building = b2.id;resource=Resource.Food;quantity=2})] ; resCarried=Map([(Resource.Food, 2)])}
        let arUB1 = {quantity=2;loc=c11.location;rType=Resource.Food;destID=b2.id}
        let c12 = {c11 with id=12; resCarried=Map.empty; tasks = Task.CollectAR(arUB1)::c11.tasks}
        //citizen building
        let c13 = {c12 with id=13; tasks=[Build(2)]}
        let gs = {GameState<int,int>.Empty() with citizens= PV.ofSeq [c0;c1;c2;c3;c4;c5;c6;c7;c8;c9;c10;c11;c12;c13]; buildings = PV.ofSeq [b0;b1;b2;b3]; resourcesAvailable = Map([(Resource.Food, Map([(0,3);(1,6)]))]) }

        Test "If task idle then return input gamestate" ((CancelTaskFunc 0 gs) = gs)

        Test "If task empty then return input gamestate with idle list" ((CancelTaskFunc 1 gs) = {gs with citizens = (gs.citizens |> (PV.update 1 {c1 with tasks = [Task.Idle]}))})
        
        Test "If producing, then remove 1 from producing, and set citizen tasks to idle" (CancelTaskFunc 2 gs = {gs with citizens = (gs.citizens |> (PV.update 2 {c2 with tasks = [Task.Idle]}));
                                                                                                                            buildings= gs.buildings |> PV.update 0 (IO({i0 with inProgress = 4}))})

        Test "If collecting, then remove 1 from being collected and new task is idle" (CancelTaskFunc 3 gs = { gs with citizens = (gs.citizens |> (PV.update 3 {c3 with tasks = [Task.Idle]}));
                                                                                                                        resourcesAvailable = Map([(Resource.Food, Map([(0,8);(1,6)]))])
                                                                                                                })
    
        let del_gs1 = {gs with citizens=(gs.citizens |> (PV.update 4 {c4 with tasks = [Task.Idle]}))} 
                        |> SetGSBld 0 (IO({i0 with enRoute=3})) 
        Test "If deliver and not in inventory, then not removed and just set to idle, enRoute also removed" 
            ((CancelTaskFunc 4 gs) = del_gs1 )
        
        let del_gs2 = {gs with citizens=(gs.citizens |> (PV.update 5 {c5 with tasks = [Task.Idle]; resCarried=Map([(Resource.RawFood, 6)])}));
                               buildings = gs.buildings |> PV.update 0 (IO({i0 with priorityTasks=[PriorityTask.CollectAR({loc=c5.location; rType=Resource.RawFood; destID=b0.id; quantity=5})]}));
                               events = [ResourceDropped({loc=c5.location; rType=Resource.RawFood; destID=b0.id; quantity=5})]
                               }
        Test "If deliver, resource in inventory, removed from inventory, event raised for res dumped, logged in business priority task to collect" 
            ((CancelTaskFunc 5 gs) = del_gs2)

        Test "Recurs through whole list" 
            ((CancelTaskFunc 6 gs) = {gs with   citizens = (gs.citizens |> (PV.update 6 {c4 with tasks = [Task.Idle]; id=6}));
                                                buildings = gs.buildings |> PV.update 0 (IO({i0 with enRoute=3}));
                                                resourcesAvailable = Map([(Resource.Food, Map([(0,8);(1,6)]))])})
        
        let ar_gs = { gs with
                            citizens = (gs.citizens |> (PV.update 7 {c4 with tasks = [Task.Idle]; id=7})); 
                            buildings = gs.buildings |> PV.update 0 (IO({i0 with priorityTasks=[PriorityTask.CollectAR(ar)]}));
                            //events = [ResourceDropped(ar)]
        }
        Test "When a AbandonedResource task is cancelled, it is stored in the to building as a priority task, 
                and event raised" 
            ((CancelTaskFunc 7 gs) = ar_gs)



        //enroute removed from both sum and invididual
        //saved for target updates
        //citizen is idle
        let gs_wh1 = CancelTaskFunc 8 gs
        let gs_wh1_sol = gs 
                            |> SetGSCit 8 {c8 with tasks=[Task.Idle]}
                            |> SetGSBld 1 (WH(i1 |> WHRemER Resource.Food 3))
        TestEq "WH cancels delivery without abandoned resource when not already picked up"
            gs_wh1
            gs_wh1_sol

        //Everything stays the same except for task added to queue to collect
        //citizen is idle
        //event is raised
        let gs_wh2 = CancelTaskFunc 9 gs
        let arwh2 = {arWH with loc=c9.location} //Abandonied resource should appear where teh citizen is when it is dropped
        let gs_wh2_sol = gs
                            |> SetGSCit 9 {c9 with tasks=[Task.Idle]; resCarried= Map([(Resource.Food, 1)])}
                            |> SetGSBld 1 (WH({i1 with priorityTasks= PriorityTask.CollectAR(arwh2) :: i1.priorityTasks}))
                            |> AddEvent (ResourceDropped(arwh2))
        TestEq "WH sucessfully adds abandoned resource if resource already collected" 
            gs_wh2
            gs_wh2_sol

        //goes to queue
        let gs_wh3 = CancelTaskFunc 10 gs
        let gs_wh3_sol = gs 
                            |> SetGSCit 10 {c10 with tasks=[Task.Idle]}
                            |> SetGSBld 1 (WH({i1 with priorityTasks = PriorityTask.CollectAR(arWH) :: i1.priorityTasks})) 
                            
        TestEq "Cancelling an AR for a WH works correctly"
            gs_wh3
            gs_wh3_sol

        let gs_ub1 = CancelTaskFunc 11 gs
        
        let gs_ub1_sol = gs 
                            |> SetGSCit 11 {c11 with tasks=[Idle]; resCarried=c11.resCarried.Add(Resource.Food,0)}
                            |> SetGSBld 3 (Blder({b3i with priorityTasks=[PriorityTask.CollectAR(arUB1)]}).sID(3))
                            |> AddEvent(ResourceDropped(arUB1))
        TestEq "Cancelling a delivery puts it in the priority queue for the builder building it if citizen has resource in inv" 
            gs_ub1
            gs_ub1_sol            

        let gs_ub2 = CancelTaskFunc 12 gs
        let gs_ub2_sol = gs
                            |> SetGSCit 12 {c12 with tasks=[Idle]}
                            |> SetGSBld 3 (Blder({b3i with priorityTasks=[PriorityTask.CollectAR(arUB1)]}).sID(3))
                            
                            // |> SetGSBld 2 (NotBuilt(b2i |> NBRemER Resource.Food 2 ,b2b))

        TestEq "Cancelling an abandoned resource and then delivery removes it form enroute (and when not in inventory)" 
            gs_ub2
            gs_ub2_sol


        let gs_ub3 = CancelTaskFunc 13 gs
        let gs_ub3_sol = gs |> SetGSCit 13 {c13 with tasks=[Idle]} 

        TestEq "cancelling a build just sets it as idle for the citizen" 
            gs_ub3
            gs_ub3_sol

        PrintTests()



type UnitTests =
    static member run() =
        testCancelTaskFunc.Test()
