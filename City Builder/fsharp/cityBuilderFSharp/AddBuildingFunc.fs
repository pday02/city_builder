﻿module AddBuildingFunc

open GameState
open Global.Tester
open Buildingns
open Buildingns.funcs
open Citizenns
open Global.Array
open Global.PV
open Global.General
open GameState.funcs


type NewEmpFindFType<'L,'P> = GameState<'L,'P> -> int -> int list

let rec private GetNewEmpsAux (cits:Citizen<'P> PV) (reqd:int) (current: int list) (i:int)=
    if (reqd = current.Length || i = cits.Length) then current
    else if cits.[i].empStatus = EmpStatus.Seeking then GetNewEmpsAux cits reqd (cits.[i].id :: current) (i+1)
    else GetNewEmpsAux cits reqd current (i+1)

/// <summary>
/// Gets new employees for a business.
/// </summary>
/// <param name="gs">The current gamestate</param>
/// <param name="reqd">The number of employees required</param>
/// Returns a list of new employee id's
let GetNewEmps (gs:GameState<'L,'P>) (reqd:int) = 
    GetNewEmpsAux gs.citizens reqd List.empty 0

let private AssignCit bldID gameS citID =
        let c = GetGSCit citID gameS
        let b = GetGSBld bldID gameS
        let e = b |> BldToEmp
        gameS   |> SetGSCit citID {c with empStatus=Working(bldID)}
                |> AddEvent (AssignedWork({c with empStatus=Working(bldID)}))
                |> SetGSBld bldID ((e |> EmpSetCore {e.core with emps=citID ::e.emps}) |> EmpToBld)

/// <summary>
/// Adds employees to a building, up to the lesser of the cap or the max
/// </summary>
/// <param name="bid">The id of the building to find employees for</param>
/// <param name="gs">The current gamestate</param>
let AddEmpsToBld (bid:int) (gs:GameState<'L,'P>) =
    match gs |> GetGSBld bid |> BldToEmpTry with
    | Some(emps) ->
        let n = (min emps.empCap emps.empMax) - emps.emps.Length
        if (n>0) 
            then
                let newEmps = GetNewEmps gs n 
                newEmps |> List.fold(AssignCit bid) gs 
            else gs
    | None -> gs

/// <summary>
/// Adds a building to the gamestate
/// </summary>
/// <param name="b">The building to add</param>
/// <param name="gs">the current gamestate</param>
let addBuilding getNewEmpsF (b: Building<'L,'P>) (gs: GameState<'L,'P>) =

    let UpdateEvent id gs =
        {gs with events = gs.events |> List.map(fun e -> match e with | NewBuilding(b) when b.id = id -> NewBuilding(GetGSBld id gs) | a -> a)}

    match BldToEmpTry b with
    | Some(emp) ->
        let e = emp.core
        if e.empMax < 0 then failwith "Building has negative max employee capacity"
        if e.emps.Length > 0 then failwith "Building being added already has employees" 
        let newID = gs.buildings.Length
        {gs with buildings=PV.conj (b.sID newID) gs.buildings}  
            |> AddEvent (NewBuilding(b.sID newID))
            |> AddEmpsToBld newID
            |> UpdateEvent newID

    | None -> 
        let newB = b |> BldSetID gs.buildings.Length
        let newEmps = NewBuilding(newB) :: gs.events
        let newBuilt = match newB with | NotBuilt(_) as nb -> nb.id :: gs.beingBuilt | _ -> gs.beingBuilt 
        {gs with 
            buildings = PV.conj newB gs.buildings
            events = NewBuilding(newB) :: gs.events
            beingBuilt = newBuilt}

type private testAddCitizen() =
    static member Test() =
        ResetTests()
        SetTestName("AddBuilding function")
        let gs1 = GameState<int,int>.Empty()
        let i1 = {IOinfo.Default(0,0) with e = {BldEmpl.Default with empCap=5; empMax=5}}
        let b1 = IO(i1)
        
        let gs2 = addBuilding GetNewEmps b1 gs1
        let b2 = IO({i1 with e = {i1.e with empMax=10; empCap=5}})        
        
        let gs3 = addBuilding GetNewEmps b2 gs2
        Test "Adding a building to an empty building list makes it the building list" (gs2.buildings = PV.ofSeq [|b1.sID(0)|])
        Test "Adding a building to a non empty building list adds it to the end of the list" (gs3.buildings = PV.ofSeq [|b1.sID(0);b2.sID(1)|])
        Test "Adding a building to an empty new building list makes it to the new building list" (gs2.events = [NewBuilding(b1.sID(0))])
        Test "Adding a building to a non empty new building list is added to the list" (gs3.events = [NewBuilding(b2.sID(1));NewBuilding(b1.sID(0))])
        Test "Only the event list and building list is altered" ({gs2 with buildings=PV.empty; events = List.empty} = gs1 && {gs3 with buildings=PV.empty; events = List.empty} = gs1 )
        
        //Add building: get employees
        let c1 = {Citizen<int>.Default(0) with name="one";id=0;empStatus=EmpStatus.Seeking}
        let c2 =  {Citizen<int>.Default(0) with name="two";id=1;empStatus=EmpStatus.Unemployed}
        let c3 =  {Citizen<int>.Default(0) with name="three";id=2;empStatus=EmpStatus.Working(10)}
        let c4 =  {Citizen<int>.Default(0) with name="four";id=3;empStatus=EmpStatus.Seeking}
        let c5 =  {Citizen<int>.Default(0) with name="five";id=4;empStatus=EmpStatus.Seeking}

        let gs4 = {GameState<int,int>.Empty() with citizens=PV.ofSeq [|c1;c2;c3;c4;c5|]; buildings=PV.ofSeq [|b1 |> BldToEmp |> EmpSetEmpMax 0 |> EmpToBld|]}
        let gs5 = addBuilding GetNewEmps b1 gs4
        let gs6 = addBuilding GetNewEmps (b1 |> BldToEmp |> EmpSetEmpMax 2 |>EmpToBld) gs4
        TestEq "When a building is added, it assigned employees if possible: 1" 
            gs5.buildings.[1]
            (IO({i1 with b = {i1.b with id=1}; e= {i1.e with emps = [0;3;4]}}))

        TestEq "When a building is added, it assigned employees if possible: 2" 
            gs5.citizens.[0].empStatus
            (Working(1))

        TestEq "When a building is added, it assigned employees if possible: 3" 
            gs5.citizens.[3].empStatus
            (Working(1))

        TestEq "When a building is added, it assigned employees if possible: 4" 
            gs5.citizens.[4].empStatus
            (Working(1))

        TestEq "Only the required number of employees are selected: 1" 
            gs6.buildings.[1]
            (IO({i1 with e = {i1.e with emps=[0;3]; empMax=2}; b={i1.b with id=1}}))

        TestEq "Only the required number of employees are selected: 2" 
            gs6.citizens.[0].empStatus 
            (Working(1))

        TestEq "Only the required number of employees are selected: 3" 
            (gs6.citizens.[3].empStatus) 
            (Working(1))

//        TestEq "Only the required number of employees are selected" (gs6.buildings.[1] = IO({i1 with e = {i1.e with emps=[3;0]; empMax=2}; b={i1.b with id=1}})
//                                                                            && gs6.citizens.[0].empStatus = Working(1)
//                                                                            && gs6.citizens.[3].empStatus = Working(1))
        
        let gs7 = addBuilding GetNewEmps (IO({i1 with e={i1.e with empCap = 1;empMax=2}})) gs4
        Test "Only the cap amount of employees are taken if lower than max" (gs7.buildings.[1] = IO({i1 with e={i1.e with emps=[0]; empMax=2; empCap=1}; b={i1.b with id=1}})
                                                                            && gs7.citizens.[0].empStatus = Working(1))
        
        Test "Events updated for citizens that find jobs when building added" (Contains (AssignedWork({c1 with empStatus=Working(1)})) (List.toArray gs5.events)
                                                                                && Contains (AssignedWork({c4 with empStatus=Working(1)})) (List.toArray gs5.events)
                                                                                && Contains (AssignedWork({c5 with empStatus=Working(1)})) (List.toArray gs5.events))
        
        let testAddNotBuilt = addBuilding GetNewEmps  ((IO({i1 with e={i1.e with empCap = 1;empMax=2}})).sConReqs Map.empty 1.0) gs3

        TestEq "If building if not built then it is added to the toBuild list" testAddNotBuilt.beingBuilt [2]

        try
            ignore(addBuilding GetNewEmps (IO({i1 with e={i1.e with empMax= -2}})) gs4)
            Test "Adding a building with max number of employees less than 0 throws an exception" false
        with
        | _ -> Test "Adding a building with max number of employees less than 0 throws an exception" true
        
        try
            ignore (addBuilding GetNewEmps (IO({i1 with e={i1.e with emps=[0]}})) gs4)
            Test "Adding a building with non empty employee array throws an exception" false
        with
        | _ -> Test "Adding a building with non empty employee array throws an exception" true

        PrintTests()


type UnitTests =
    static member run() =
        testAddCitizen.Test()

